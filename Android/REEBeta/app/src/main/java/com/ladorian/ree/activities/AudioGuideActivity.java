package com.ladorian.ree.activities;

        import android.annotation.TargetApi;
        import android.bluetooth.BluetoothAdapter;
        import android.bluetooth.BluetoothDevice;
        import android.bluetooth.BluetoothManager;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Typeface;
        import android.media.MediaPlayer;
        import android.net.Uri;
        import android.os.Build;
        import android.os.Bundle;
        import android.os.Handler;
        import android.support.v4.app.FragmentActivity;
        import android.support.v4.app.FragmentManager;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.ImageButton;
        import android.widget.ListView;
        import android.widget.SeekBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.ladorian.ree.fragments.NotificationFragment;
        import com.ladorian.ree.R;
        import com.ladorian.ree.adapters.ContentListAdapter;
        import com.ladorian.ree.helpers.AmbitoItems;
        import com.ladorian.ree.helpers.BleUtil;
        import com.ladorian.ree.helpers.ExpoBeacons;
        import com.ladorian.ree.helpers.ScannedDevice;
        import com.ladorian.ree.helpers.Utilities;
        import com.ladorian.ree.models.Content;


        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.IOException;
        import java.io.InputStream;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.Collections;
        import java.util.Comparator;


/**
 * Created by GRANBAZU on 19/5/15.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class AudioGuideActivity extends FragmentActivity implements BluetoothAdapter.LeScanCallback, MediaPlayer.OnCompletionListener,SeekBar.OnSeekBarChangeListener,AdapterView.OnItemClickListener {
    protected static final String TAG = "REE BEACONS";
    private BluetoothAdapter mBTAdapter;
    private boolean mIsScanning;
    private boolean isfirstScan;
    private String[] mMACS;
    private ArrayList<ScannedDevice> mScannedDevices = new ArrayList<>();
    private Handler stopHandler = new Handler();
    private ScannedDevice currentBeacon;
    private ScannedDevice nextBeacon;
    private JSONObject mBeaconList;
    private FragmentManager fm;

    private TextView mAmbitoLabel;
    private TextView mContentLabel;
    private ListView mListView;
    //private BaseAdapter mAdapter;
    private Typeface miso_font;
    private int currentMultiIndex = 0;
    private boolean isShowingMulti = false;

    private MediaPlayer mMediaPlayer;
    private String currentAudio;
    private static SeekBar mSeekBar;
    private ImageButton playBtn;
    private Handler mHandler = new Handler();
    private Utilities utils;
    private ContentListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        utils = new Utilities();
        miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
        try {
            mBeaconList = new JSONObject(loadJSONFromAsset());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mMACS = ExpoBeacons.getMACS();
        setContentView(R.layout.audioguide);
        getActionBar().hide();
        mAmbitoLabel = (TextView) findViewById(R.id.ambito);
        mContentLabel = (TextView)findViewById(R.id.title);
        mAmbitoLabel.setTypeface(miso_font);
        mContentLabel.setTypeface(miso_font);
        mListView = (ListView)findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        playBtn = (ImageButton)findViewById(R.id.play_audio);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                useAudio();
                updatePlayBtn();
            }
        });
        playBtn.setBackgroundResource(getBackgroundButtonRes("ambito1"));
        Button closeBtn = (Button)findViewById(R.id.closeConfig);
        closeBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        updatePlayBtn();
        mSeekBar = (SeekBar) findViewById(R.id.audioSeek);
        mSeekBar.setOnSeekBarChangeListener(this);
        updateProgressBar();

        init();
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is;
            is = getAssets().open("beacon_list.json");
            //is = getAssets().open("test_beacon_list.json");
            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mIsScanning) {
            mIsScanning = false;
            stopHandler.removeCallbacks(stopScanRunnable);
            fullStopScan();
        }
        /*if(mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mIsScanning) {
            mIsScanning=false;
            stopHandler.removeCallbacks(stopScanRunnable);
            fullStopScan();
        }
        if(mMediaPlayer!=null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        if(checkExpoBeacon(device)) {
           // Log.i("EXPO BEACON DETECTED", "mac: " + device.getAddress() + " rssi: " + Integer.toString(rssi));
            mScannedDevices.add(new ScannedDevice(device,rssi,scanRecord));
            if(isfirstScan) {
                stopHandler.postDelayed(stopScanRunnable,5000);
                isfirstScan = false;
            }
        } else {
            //Log.i("NO EXPO BEACON", "mac: " + device.getAddress() + " rssi: " + Integer.toString(rssi));
        }
    }

    private Boolean checkExpoBeacon (BluetoothDevice device) {
        return Arrays.asList(mMACS).contains(device.getAddress());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void init() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2){
            if (!BleUtil.isBLESupported(this)) {
                Toast.makeText(this, "No compatible con BLE", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }

            // BT check
            BluetoothManager manager = BleUtil.getManager(this);
            if (manager != null) {
                mBTAdapter = manager.getAdapter();
            }
            if (mBTAdapter == null) {
                Toast.makeText(this, "No compatible con Bluetooth", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            isfirstScan = true;
            startScan();
        } else{
            Toast.makeText(this, "Mínimo Android 4.3 JELLYBEAN", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        // BLE check

    }

    private void startScan() {
        if ((mBTAdapter != null) && (!mIsScanning)) {
            mBTAdapter.startLeScan(this);
            mIsScanning = true;
            //setProgressBarIndeterminateVisibility(true);
        }
    }

    private void fullStopScan() {
        if (mBTAdapter != null) {
            mBTAdapter.stopLeScan(this);
        }
    }

    private void stopScan() {
        if (mBTAdapter != null) {
            mBTAdapter.stopLeScan(this);
        }
        mIsScanning = false;
        orderScanned();
        isfirstScan = true;
        //Log.i("REE", "Closest Beacon: " + mScannedDevices.get(0).getDevice().getAddress());
        //setProgressBarIndeterminateVisibility(false);
        setBeacons();
    }

    private void setBeacons(){
        if(mScannedDevices.size()>0) {
            if (currentBeacon == null) {
                setCurrentBeacon(mScannedDevices.get(0));
            }
            else {
                //Si el beacon más cercano no es el currentBeacon
                if(!currentBeacon.getDevice().getAddress().equals(mScannedDevices.get(0).getDevice().getAddress())) {
                    //Si no hay nextBeacon o el bracon más cercano no es ya el nextBeacon
                    if(nextBeacon==null || !nextBeacon.getDevice().getAddress().equals(mScannedDevices.get(0).getDevice().getAddress())){
                        setNextBeacon(mScannedDevices.get(0));
                    }
                }
            }

        }
        mScannedDevices.clear();
        startScan();
    }

    private void setNextBeacon(ScannedDevice beacon) {
        nextBeacon = null;
        nextBeacon = beacon;
        //Log.i("REE", "Next Beacon: " + nextBeacon.getDevice().getAddress());
        try {
            JSONObject beaconData = mBeaconList.getJSONObject(nextBeacon.getDevice().getAddress());
            //Log.i("REE", "Next Data: " + beaconData.toString());
            loadNotification(beaconData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadNotification(JSONObject data) {
        boolean isFirst = false;
        boolean isLast = false;
        boolean isMulti = false;
        String notificationTitle;
        try {
            AmbitoItems ambitoData = new AmbitoItems(this, data.getInt("ambito"));
            if(!data.getBoolean("multi")) {
                isFirst = false;
                isLast = false;
                notificationTitle = ambitoData.ambito.getContents().get(data.getInt("position")).getTitle();
                isMulti = false;
                NotificationFragment f = NotificationFragment.newInstance(isMulti, notificationTitle, isFirst, isLast, data.getInt("ambito"));
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.notification, f, "NOTIFICATION")
                        .commit();
            } else {
                notificationTitle = ambitoData.ambito.getContents().get(data.getJSONArray("contents").getInt(currentMultiIndex)).getTitle();
                if(currentMultiIndex==0) {
                    isMulti = true;
                    isFirst = true;
                    isLast = false;
                } else if (currentMultiIndex==data.getJSONArray("contents").length()-1) {
                    isMulti = true;
                    isFirst = false;
                    isLast = true;
                } else {
                    isMulti = true;
                    isFirst = false;
                    isLast = false;
                }
                if(currentMultiIndex<=data.getJSONArray("contents").length()-1) {
                    NotificationFragment f = NotificationFragment.newInstance(isMulti, notificationTitle, isFirst, isLast, data.getInt("ambito"));
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.notification, f, "NOTIFICATION")
                            .commit();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setCurrentBeacon (ScannedDevice beacon) {
        currentBeacon = null;
        currentBeacon = beacon;
        if(nextBeacon!=null && currentBeacon.getDevice().getAddress().equals(nextBeacon.getDevice().getAddress())) {
            nextBeacon=null;
        }
        //Log.i("REE","Current Beacon: "+currentBeacon.getDevice().getAddress());
        try {
            JSONObject beaconData = mBeaconList.getJSONObject(currentBeacon.getDevice().getAddress());
            //Log.i("REE", "Current Data: " + beaconData.toString());
            updateContent(beaconData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateContent(JSONObject data) {
        if(mMediaPlayer!=null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            currentAudio = "";
            mMediaPlayer.release();
        }
        mSeekBar.setProgress(0);
        playBtn.setImageResource(R.drawable.play_btn);

        Content apartado;
        try {
            //currentMultiIndex=0;
            mMediaPlayer = new MediaPlayer();
            AmbitoItems ambitoData = new AmbitoItems(this, data.getInt("ambito"));
            mAmbitoLabel.setText(ambitoData.ambito.getTitle());
            if(!data.getBoolean("multi")) {
                apartado = ambitoData.ambito.getContents().get(data.getInt("position"));
                mContentLabel.setText(ambitoData.ambito.getContents().get(data.getInt("position")).getTitle());
                currentAudio = ambitoData.ambito.getContents().get(data.getInt("position")).getAudio();
                isShowingMulti = false;
                currentMultiIndex=0;
                NotificationFragment f = (NotificationFragment) getSupportFragmentManager().findFragmentByTag("NOTIFICATION");
                if(f!=null) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .remove(f)
                            .commit();
                }

            } else {
                apartado = ambitoData.ambito.getContents().get(data.getJSONArray("contents").getInt(currentMultiIndex));
                mContentLabel.setText(ambitoData.ambito.getContents().get(data.getJSONArray("contents").getInt(currentMultiIndex)).getTitle());
                currentAudio = ambitoData.ambito.getContents().get(data.getJSONArray("contents").getInt(currentMultiIndex)).getAudio();
                isShowingMulti = true;
                loadNextNotification();
            }
            int colorcode;

            switch (data.getInt("ambito")) {
                case 0:
                    colorcode = getResources().getColor(R.color.ambito1);
                    playBtn.setBackgroundResource(getBackgroundButtonRes("ambito1"));
                    break;
                case 1:
                    colorcode = getResources().getColor(R.color.ambito2);
                    playBtn.setBackgroundResource(getBackgroundButtonRes("ambito2"));
                    break;
                case 2:
                    colorcode = getResources().getColor(R.color.ambito3);
                    playBtn.setBackgroundResource(getBackgroundButtonRes("ambito3"));

                    break;
                case 3:
                    colorcode = getResources().getColor(R.color.ambito4);
                    playBtn.setBackgroundResource(getBackgroundButtonRes("ambito4"));

                    break;
                default:
                    colorcode = getResources().getColor(R.color.ambito1);
                    playBtn.setBackgroundResource(getBackgroundButtonRes("ambito1"));

                    break;
            }
            mAmbitoLabel.setTextColor(colorcode);
            mContentLabel.setTextColor(colorcode);

            if(mAdapter !=null) {
                mAdapter = null;
            }
                if(apartado.getText().equals("NO")) {
                    mAdapter = new ContentListAdapter(null, null,apartado.getContents(),this,ambitoData.ambito.getBackground());
                } else {
                    mAdapter = new ContentListAdapter(apartado.getText(), apartado.getTitle(),apartado.getContents(),this,ambitoData.ambito.getBackground());
                }

            mListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();


        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadNextBeaconData(){
        //currentMultiIndex=0;
        NotificationFragment f = (NotificationFragment) getSupportFragmentManager().findFragmentByTag("NOTIFICATION");
        if(f!=null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(f)
                    .commit();
        }
        setCurrentBeacon(nextBeacon);
        //nextBeacon = null;

    }

    public void loadNextMultiContent(){
        try {
            if(isShowingMulti) {
                JSONObject beaconData = mBeaconList.getJSONObject(currentBeacon.getDevice().getAddress());
                updateContent(beaconData);
                loadNextNotification();
            } else {
                loadNextBeaconData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void loadNextNotification(){
        NotificationFragment f = (NotificationFragment) getSupportFragmentManager().findFragmentByTag("NOTIFICATION");
        if(f!=null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(f)
                    .commit();
        }
            currentMultiIndex = currentMultiIndex+1;
        try {
            if(isShowingMulti) {
                JSONObject beaconData = mBeaconList.getJSONObject(currentBeacon.getDevice().getAddress());
                //Log.i("REE", "Next Data: " + beaconData.toString());
                loadNotification(beaconData);
            } else {
                JSONObject beaconData = mBeaconList.getJSONObject(nextBeacon.getDevice().getAddress());
                //Log.i("REE", "Next Data: " + beaconData.toString());
                loadNotification(beaconData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadPrevNotification(){
        NotificationFragment f = (NotificationFragment) getSupportFragmentManager().findFragmentByTag("NOTIFICATION");
        if(f!=null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(f)
                    .commit();
        }
            currentMultiIndex = currentMultiIndex-1;
        try {
            if(isShowingMulti){
                JSONObject beaconData = mBeaconList.getJSONObject(currentBeacon.getDevice().getAddress());
                //Log.i("REE", "Next Data: " + beaconData.toString());
                loadNotification(beaconData);
            } else {
                JSONObject beaconData = mBeaconList.getJSONObject(nextBeacon.getDevice().getAddress());
                //Log.i("REE", "Next Data: " + beaconData.toString());
                loadNotification(beaconData);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void orderScanned () {
        Collections.sort(mScannedDevices, new Comparator<ScannedDevice>() {
            @Override
            public int compare(ScannedDevice lhs, ScannedDevice rhs) {
                return new Integer(rhs.getRssi()).compareTo(new Integer(lhs.getRssi()));
            }
        });
    }

    private Runnable stopScanRunnable = new Runnable() {
        @Override
        public void run() {
            if(mScannedDevices != null && mScannedDevices.size()>0) {
                stopScan();
            } else {
                stopHandler.postDelayed(this,1000);
            }
        }
    };


    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.release();
        mMediaPlayer = null;
    }

    public void useAudio() {
        if(currentAudio == null || currentAudio.equals("")){

        } else {

            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            } else {
                //this.currentAudio = audio;
                playAudio(currentAudio);
            }
        }


    }

    public void playAudio (String filename) {
        /*if(mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
        }*/
        try {
            mMediaPlayer.reset();
            mMediaPlayer.setOnCompletionListener(this);
            String uriPath = "android.resource://"+getPackageName()+"/raw/"+filename;
            Uri uri = Uri.parse(uriPath);
            mMediaPlayer.setDataSource(this,uri);
            mMediaPlayer.prepare();
            mMediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updatePlayBtn () {
        MediaPlayer mp = mMediaPlayer;
        if (mp!=null && mp.isPlaying() /*&& getCurrentAudio().equals(audioName)*/) {
            playBtn.setImageResource(R.drawable.pause_btn);
        } else {
            playBtn.setImageResource(R.drawable.play_btn);
        }
    }


    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask,100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        @Override
        public void run() {
            MediaPlayer mp = mMediaPlayer;
            if (mp!=null && mp.isPlaying() /*&& getCurrentAudio().equals(audioName)*/) {
                long totalDuration = mMediaPlayer.getDuration();
                long currentDuration =mMediaPlayer.getCurrentPosition();
                mSeekBar.setProgress(utils.getProgressPercentage(currentDuration,totalDuration));
            }
            mHandler.postDelayed(this,100);
        }
    };

    ///SEEKBARMETHODS

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        MediaPlayer mp = mMediaPlayer;
        if(mp!=null) {
            mHandler.removeCallbacks(mUpdateTimeTask);
            int totalDuration = mp.getDuration();
            int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

            // forward or backward to certain seconds
            mp.seekTo(currentPosition);

            // update timer progress again
            updateProgressBar();
        } else {
            mSeekBar.setProgress(0);
        }
    }

    public String getCurrentAudio() {
        return currentAudio;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        JSONObject item = mAdapter.getItem(position);
        try {
            if(item.getString("tipo").equals("mp4")) {
                MediaPlayer mp = mMediaPlayer;
                if (mp!=null && mp.isPlaying()) {
                    mp.pause();
                }
                Context context = getApplicationContext();
                Intent intent = null;
                intent = new Intent(context,VideoPlayerActivity.class);
                intent.putExtra("video", item.getString("archivo"));
                startActivity(intent);
            } else if (item.getString("tipo").equals("PDF")) {
                Context context = getApplicationContext();
                Intent intent = null;
                intent = new Intent(context,PDFactivity.class);
                intent.putExtra("pdf",item.getString("archivo")+".pdf");
                startActivity(intent);
            } else if (item.getString("tipo").equals("TXT") || item.getString("tipo").equals("INF")) {
                Context context = getApplicationContext();
                Intent intent = null;
                intent = new Intent(context,TextActivity.class);
                intent.putExtra("texto",item.getString("archivo"));
                intent.putExtra("titulo",item.getString("titulo"));
                startActivity(intent);
            } else if (item.getString("tipo").equals("WEB")) {
                String url = item.getString("archivo");
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private int getBackgroundButtonRes(String backgroundName){
        int res_button;
        String resName = backgroundName +"_oval_button";
        res_button = this.getResources().getIdentifier(resName, "drawable", this.getPackageName());
        return res_button;
    }
}
