package com.ladorian.ree.activities;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ladorian.ree.fragments.Q2SecondFragment;
import com.ladorian.ree.fragments.Q2ThirdFragment;
import com.ladorian.ree.fragments.Q2WelcomeFragment;
import com.ladorian.ree.fragments.QBulbFragment;
import com.ladorian.ree.fragments.QFinalFragment;
import com.ladorian.ree.fragments.Q2FirstFragment;
import com.ladorian.ree.fragments.QFirstFragment;
import com.ladorian.ree.fragments.QMultiFragment;
import com.ladorian.ree.R;
import com.ladorian.ree.helpers.ApiClient;
import com.ladorian.ree.helpers.SurveySender;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.helpers.WaitingDialog;
import com.ladorian.ree.models.Cuestionario;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CuestionarioActivity extends FragmentActivity implements MediaPlayer.OnCompletionListener {
    private MediaPlayer mMediaPlayer;
    public static Typeface miso_font;
    private Cuestionario mCuestionario;
    private Traducciones trad;
    private String idioma;
    Button go1;
    Button go2;
    Button go3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        trad = new Traducciones(this);
        super.onCreate(savedInstanceState);
        Intent callingIntent = getIntent();
        idioma = callingIntent.getExtras().getString("idioma");

        mMediaPlayer = new MediaPlayer();
        this.playAudio();
        mCuestionario = new Cuestionario();
        miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
        setContentView(R.layout.cuestionario_activity);
        showActionBar();
//        TextView title = (TextView) findViewById(R.id.tituloGeneral);
//        title.setTypeface(miso_font);
//        title.setText(trad.getTexto("Una autopista detras del enchufe"));
        TextView subTitle = (TextView) findViewById(R.id.title);
        subTitle.setTypeface(miso_font);
        subTitle.setText(trad.getTexto("Encuesta"));

        go1 = (Button) findViewById(R.id.goSection1);
        go2 = (Button) findViewById(R.id.goSection2);
        go3 = (Button) findViewById(R.id.goSection3);
        go1.setVisibility(View.INVISIBLE);
        go2.setVisibility(View.INVISIBLE);
        go3.setVisibility(View.INVISIBLE);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new Q2WelcomeFragment())
                    .commit();
        }
        configurarNavegacion();

    }

    public void showActionBar() {
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.action_bar_title, null);
        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setCustomView(v);
        ImageView logoBtn = (ImageView) findViewById(R.id.logo);
        logoBtn.setScaleType(ImageView.ScaleType.FIT_START);
//        layoutTitulo = (LinearLayout) findViewById(R.id.layoutTitulo);
        ImageButton leftBtn = (ImageButton) findViewById(R.id.btn1);
        leftBtn.setVisibility(View.GONE);

        ImageButton rightBtn = (ImageButton) findViewById(R.id.btn2);
        rightBtn.setVisibility(View.GONE);
        miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
        TextView tituloGeneral = (TextView) findViewById(R.id.tituloGeneral);
        tituloGeneral.setTypeface(miso_font);
        tituloGeneral.setText(trad.getTexto("Una autopista detras del enchufe"));
    }

    private void configurarNavegacion() {
        go1 = (Button) findViewById(R.id.goSection1);
        go1.setText(trad.getTexto("Perfil"));
        go1.setTypeface(miso_font);
        go1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeQuestion(0);
            }
        });
        go2 = (Button) findViewById(R.id.goSection2);
        go2.setText(trad.getTexto("Sobre la exposicion"));
        go2.setTypeface(miso_font);
//        go2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                changeQuestion(1);
//            }
//        });
        go3 = (Button) findViewById(R.id.goSection3);
        go3.setText(trad.getTexto("Sobre REE"));
        go3.setTypeface(miso_font);
//        go3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                changeQuestion(2);
//            }
//        });
    }

    public void playAudio() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.setOnCompletionListener(this);
                String uriPath;
                if (idioma != null) {
                    if (idioma.equals("castellano")) {
                        uriPath = "android.resource://" + getPackageName() + "/raw/a_052_despedida";
                    } else if (idioma.equals("catala")) {
                        uriPath = "android.resource://" + getPackageName() + "/raw/a_052_despedida_cat";
                    } else {
                        uriPath = "android.resource://" + getPackageName() + "/raw/a_052_despedida_eng";
                    }
                } else {
                    uriPath = "android.resource://" + getPackageName() + "/raw/a_052_despedida";
                }
                Uri uri = Uri.parse(uriPath);
                mMediaPlayer.setDataSource(this, uri);
                mMediaPlayer.prepare();
                mMediaPlayer.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.release();
        mMediaPlayer = null;
    }

    @Override
    protected void onPause() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void onBackPressed() {
        final FragmentActivity weakActivity = this;
        new AlertDialog.Builder(this)
                .setTitle(trad.getTexto("Abandonar el cuestionario"))
                .setMessage(trad.getTexto("¿Desea abandonar el cuestionario? Si lo hace, sus respuestas se borrarán y deberá volver a empezar."))
                .setPositiveButton(trad.getTexto("Abandonar"), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        weakActivity.finish();
                    }
                })
                .setNegativeButton(trad.getTexto("Continuar"), null)
                .show();
    }

    public Cuestionario getmCuestionario() {
        return mCuestionario;
    }

    public void sendCuestionario() {

//        FragmentManager fm = getSupportFragmentManager();
//        int count = fm.getBackStackEntryCount();
//        for (int i = 0; i < count; i++) {
//            fm.popBackStack();
//        }
//        final WaitingDialog wd = new WaitingDialog();
//        wd.show(fm, "waiting");
        SurveySender sender = new SurveySender();
        sender.textoOK = trad.getTexto("El cuestionario se ha enviado. Gracias por su colaboracion.");
        sender.textoNOOK = trad.getTexto("No se han podido registrar los datos.");
        Boolean resultado = sender.sendData(mCuestionario);
//        wd.dismiss();
        mCuestionario = null;
        mCuestionario = new Cuestionario();

        failAlert(sender.textoOK);
        this.finish();
    }

    public void failAlert(String message) {
        String msg = trad.getTexto("No se ha podido completar el registro. Inténtelo más tarde.");
        if (message != null) msg = message;
        new AlertDialog.Builder(this)
                .setTitle(trad.getTexto("¡Muchas Gracias!"))
                .setMessage(msg)
                .setPositiveButton(trad.getTexto("Aceptar"), null)
                .show();
    }

    public void changeQuestion(int question) {
        Fragment fragment = null;
        switch (question) {
            case 0:
                fragment = new Q2FirstFragment();
                go1.setVisibility(View.VISIBLE);
                go2.setVisibility(View.VISIBLE);
                go3.setVisibility(View.VISIBLE);
                go1.setBackground(getResources().getDrawable(R.drawable.q_button_slim));
                go2.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                go3.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                break;
            case 1:
                fragment = new Q2SecondFragment();
                go1.setVisibility(View.VISIBLE);
                go2.setVisibility(View.VISIBLE);
                go3.setVisibility(View.VISIBLE);
                go1.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                go2.setBackground(getResources().getDrawable(R.drawable.q_button_slim));
                go3.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                break;
            case 2:
                fragment = new Q2ThirdFragment();
                go1.setVisibility(View.VISIBLE);
                go2.setVisibility(View.VISIBLE);
                go3.setVisibility(View.VISIBLE);
                go1.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                go2.setBackground(getResources().getDrawable(R.drawable.q_button_slim_disabled));
                go3.setBackground(getResources().getDrawable(R.drawable.q_button_slim));
                break;
            default:
                go1.setVisibility(View.INVISIBLE);
                go2.setVisibility(View.INVISIBLE);
                go3.setVisibility(View.INVISIBLE);
                break;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


}
