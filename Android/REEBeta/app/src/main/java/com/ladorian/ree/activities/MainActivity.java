package com.ladorian.ree.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.adapters.AmbitoListAdapter;
import com.ladorian.ree.adapters.ContentListAdapter;
import com.ladorian.ree.adapters.ImageSliderAdapter;
import com.ladorian.ree.adapters.MainListAdapter;
import com.ladorian.ree.fragments.AmbitoFragment;
import com.ladorian.ree.fragments.ConfigFragment;
import com.ladorian.ree.fragments.ContentsFragment;
import com.ladorian.ree.fragments.StartFragment;
import com.ladorian.ree.helpers.AmbitoItems;
import com.ladorian.ree.helpers.PlaybackService;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.helpers.Utilities;
import com.ladorian.ree.models.Ambito;
import com.ladorian.ree.models.Content;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import org.json.JSONException;
import org.json.JSONObject;

//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBar;

public class MainActivity extends FragmentActivity //implements MediaPlayer.OnCompletionListener
{

    //	private MediaPlayer mMediaPlayer;
    private String currentAudio;
    private Handler mHandler = new Handler();
    private android.app.Fragment currentFragment;
    private ImageButton leftBtn;
    private ImageButton rightBtn;
    private ImageView logoBtn;
//    private LinearLayout layoutTitulo;
//    private TextView expo;
    private TextView tituloGeneral;
    private Boolean goBack;
    public static Typeface miso_font;
    private Traducciones trad;

    public void setGoBack(Boolean goBack) {
        this.goBack = goBack;
    }

    public void setTranducciones(Traducciones newTrad){
        trad= newTrad;

    }
    @Override
    public void onBackPressed() {
        if (goBack) {
            super.onBackPressed();
        }
    }

    public String getCurrentAudio() {
        return currentAudio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        trad = new Traducciones(this);
        this.currentAudio = "";
        setContentView(R.layout.activity_main);
        showActionBar();

        if (savedInstanceState == null) {
            //getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.container, new StartFragment()).commit();
        }

    }

    public void sendPlaybackIntent(String action) {
        sendPlaybackIntent(action, null);
    }

    public void sendPlaybackIntent(String action, String uri) {
        Intent intent = new Intent(this, PlaybackService.class);
        intent.setAction(action);
        if (uri != null) {
            intent.putExtra(PlaybackService.PARAM_CONTENT_URI, uri);
        }
        startService(intent);
    }

    @Override
    protected void onPause() {
//		if (mMediaPlayer != null)
//		{
//			if (mMediaPlayer.isPlaying())
//			{
//				mMediaPlayer.pause();
//			}
//		}

        if (!ContentsFragment.isGoingToNextScreen()) {
            sendPlaybackIntent(PlaybackService.ACTION_STOP);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (!ContentsFragment.isGoingToNextScreen()) {
            sendPlaybackIntent(PlaybackService.ACTION_STOP);
        }

        super.onDestroy();
    }

    public void showActionBar() {
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.action_bar_title, null);
        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setCustomView(v);
        logoBtn = (ImageView) findViewById(R.id.logo);
//        layoutTitulo = (LinearLayout) findViewById(R.id.layoutTitulo);
        leftBtn = (ImageButton) findViewById(R.id.btn1);
        leftBtn.setVisibility(View.GONE);
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        rightBtn = (ImageButton) findViewById(R.id.btn2);
        final Context c = this;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            rightBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(c, AudioGuideActivity.class);
                    startActivity(i);
                }
            });
        } else {
            rightBtn.setVisibility(View.GONE);
        }
        miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
//        expo = (TextView) findViewById(R.id.expo);
//        expo.setTypeface(miso_font);
//        expo.setText(trad.getTexto("EXPOSICION"));
        tituloGeneral = (TextView) findViewById(R.id.tituloGeneral);
        tituloGeneral.setTypeface(miso_font);
        tituloGeneral.setText(trad.getTexto("Una autopista detras del enchufe"));
    }

    public void loadConfig() {
        ConfigFragment fragment = new ConfigFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }
    public void goMainMenu() {
        ConfigFragment fragment = new ConfigFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }
    public void goBack() {
     onBackPressed();
    }
    public void hideAllBar() {
        logoBtn.setVisibility(View.INVISIBLE);
        leftBtn.setVisibility(View.GONE);
      //  logoBtn.setScaleType(ImageView.ScaleType.FIT_START);

        tituloGeneral.setVisibility(View.INVISIBLE);
//        expo.setVisibility(View.GONE);
    }
    public void showAllBar() {
        logoBtn.setVisibility(View.VISIBLE);
        leftBtn.setVisibility(View.GONE);
        //  logoBtn.setScaleType(ImageView.ScaleType.FIT_START);

        tituloGeneral.setVisibility(View.VISIBLE);
//        expo.setVisibility(View.GONE);
    }
    public void hideConfigBtn() {
        leftBtn.setVisibility(View.GONE);
        logoBtn.setScaleType(ImageView.ScaleType.FIT_START);
//        layoutTitulo.setVisibility(View.VISIBLE);
    }

    public void showConfigBtn() {
        leftBtn.setVisibility(View.GONE);
        logoBtn.setScaleType(ImageView.ScaleType.FIT_START);
       // layoutTitulo.setVisibility(View.INVISIBLE);
    }

    public void hideGuideBtn() {
        rightBtn.setVisibility(View.GONE);
    }

    public void showGuideBtn() {
        rightBtn.setVisibility(View.GONE);
    }

    public void hideButtons() {
        hideGuideBtn();
        hideConfigBtn();
        logoBtn.setScaleType(ImageView.ScaleType.FIT_START);
//        layoutTitulo.setVisibility(View.VISIBLE);
    }

    public void showButtons() {
        showGuideBtn();
        showConfigBtn();
        logoBtn.setScaleType(ImageView.ScaleType.FIT_START);
//        layoutTitulo.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(int groupPosition) {
        AmbitoFragment fragment = new AmbitoFragment();
        fragment.setnAmb(groupPosition);
       changeFragment(fragment);
    }
    public void changeFragment(Fragment newFragment) {
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();

        ft.replace(R.id.container, newFragment);
        ft.addToBackStack(null);
        ft.commit();

    }
    public void refreshFragment(Fragment fragment){
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(fragment);
        ft.attach(fragment);
        ft.commit();
    }
    public void loadContents(AmbitoItems ambito, int position) {

        ContentsFragment fragment = new ContentsFragment();
        fragment.setAmbito(ambito);
        fragment.setPosition(position);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    public void useAudio(String audio) {
        if (PlaybackService.getMediaPlayer() == null) {
            this.currentAudio = audio;
            this.playAudio(audio);
        } else {
            if (this.currentAudio.equals(audio)) {
                MediaPlayer mp = PlaybackService.getMediaPlayer();
//				if (mp.isPlaying())
                if (PlaybackService.isPlaying()) {
                    mp.pause();
                } else {
                    this.currentAudio = audio;
                    mp.start();
                }
            } else {
                this.currentAudio = audio;
                this.playAudio(audio);
            }
        }
    }

    public void changeLanguage(String newLanguage, Fragment fragment){
        SharedPreferences.Editor editor = this.getSharedPreferences("REE_PREFS", Context.MODE_PRIVATE).edit();
        editor.putString("idioma", newLanguage);
        editor.apply();
        setTranducciones(new Traducciones(this));
        if(fragment!=null) {
            refreshFragment(fragment);
        }
        showActionBar();

    }
    public void playAudio(String filename) {
        String uriPath = "android.resource://" + getPackageName() + "/raw/" + filename;
        sendPlaybackIntent(PlaybackService.ACTION_PLAY, uriPath);
//		Uri uri = Uri.parse(uriPath);
//		if (mMediaPlayer == null)
//		{
//			mMediaPlayer = new MediaPlayer();
//		}
//		try
//		{
//			mMediaPlayer.reset();
//			mMediaPlayer.setOnCompletionListener(this);
//			mMediaPlayer.setDataSource(this, uri);
//			mMediaPlayer.prepare();
//			mMediaPlayer.start();
//
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}
    }

//	@Override
//	public void onCompletion(MediaPlayer mp)
//	{
//		mp.release();
//		mMediaPlayer = null;
//	}








}
