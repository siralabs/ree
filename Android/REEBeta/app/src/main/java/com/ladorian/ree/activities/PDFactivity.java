package com.ladorian.ree.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.ladorian.ree.R;
import com.joanzapata.pdfview.PDFView;

/**
 * Created by GRANBAZU on 26/4/15.
 */
public class PDFactivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.pdf_activity);
        PDFView pdfView = (PDFView)findViewById(R.id.pdfview);
        Intent callingIntent = getIntent();
        String pdf = (String)callingIntent.getExtras().get("pdf");
        pdfView.fromAsset(pdf)
                .showMinimap(false)
                .enableSwipe(true)
                .load();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
