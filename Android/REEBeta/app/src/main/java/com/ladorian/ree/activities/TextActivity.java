package com.ladorian.ree.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.helpers.PlaybackService;
import com.ladorian.ree.helpers.Traducciones;

/**
 * Created by GRANBAZU on 26/4/15.
 */
public class TextActivity extends Activity
{
	private Typeface miso_font;
	private TextView mContentText;
	private int mCurrentFontSize;

	private boolean mBackPressed;
	private ImageButton leftBtn;
	private ImageButton rightBtn;
	private TextView tituloGeneral;
	private Traducciones trad;
	private ImageView logoBtn;
	private static final int FONTSIZE_DEFAULT = 18;
	private static final int FONTSIZE_STEP = 2;
	private static final int FONTSIZE_MIN = FONTSIZE_DEFAULT - 2;
	private static final int FONTSIZE_MAX = FONTSIZE_DEFAULT + 20;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text_activity);
		trad = new Traducciones(this);
		miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
		showActionBar();
		Intent callingIntent = getIntent();
		String texto = (String) callingIntent.getExtras().get("texto");
		String titulo = (String) callingIntent.getExtras().get("titulo2");
		mContentText = (TextView) findViewById(R.id.texto);
		mContentText.setText(texto);
		mContentText.setTypeface(miso_font);
		mContentText.setMovementMethod(new ScrollingMovementMethod());
		TextView title = (TextView) findViewById(R.id.titulo);
		title.setTypeface(miso_font);
		title.setText(titulo);

		mCurrentFontSize = FONTSIZE_DEFAULT;
		((ImageButton) findViewById(R.id.button_reduceFont)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mCurrentFontSize = Math.max(mCurrentFontSize - FONTSIZE_STEP, FONTSIZE_MIN);
				mContentText.setTextSize(TypedValue.COMPLEX_UNIT_SP, mCurrentFontSize);
			}
		});
		((ImageButton) findViewById(R.id.button_enlargeFont)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mCurrentFontSize = Math.min(mCurrentFontSize + FONTSIZE_STEP, FONTSIZE_MAX);
				mContentText.setTextSize(TypedValue.COMPLEX_UNIT_SP, mCurrentFontSize);
			}
		});
	}

	private void showActionBar()
	{
		LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.action_bar_title, null);
		ImageButton config = (ImageButton) v.findViewById(R.id.btn1);
		config.setVisibility(View.GONE);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setCustomView(v);
		logoBtn = (ImageView) findViewById(R.id.logo);
//        layoutTitulo = (LinearLayout) findViewById(R.id.layoutTitulo);
		leftBtn = (ImageButton) findViewById(R.id.btn1);
		leftBtn.setVisibility(View.GONE);

		rightBtn = (ImageButton) findViewById(R.id.btn2);

			rightBtn.setVisibility(View.GONE);
		miso_font = Typeface.createFromAsset(getAssets(), "fonts/miso.otf");
		tituloGeneral = (TextView) findViewById(R.id.tituloGeneral);
		tituloGeneral.setTypeface(miso_font);
		tituloGeneral.setText(trad.getTexto("Una autopista detras del enchufe"));
	}

	@Override
	protected void onPause()
	{
		if (!mBackPressed)
		{
			Intent intent = new Intent(this, PlaybackService.class);
			intent.setAction(PlaybackService.ACTION_STOP);
			startService(intent);
		}

		super.onPause();
	}

	@Override
	public void onBackPressed()
	{
		mBackPressed = true;
		super.onBackPressed();
	}
}
