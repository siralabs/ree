package com.ladorian.ree.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.MediaController;
import android.os.Bundle;
import android.widget.VideoView;

import com.ladorian.ree.R;

import java.io.IOException;

/**
 * Created by GRANBAZU on 24/4/15.
 */
public class VideoPlayerActivity extends Activity implements MediaPlayer.OnPreparedListener {
    private VideoView videoView;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.video_player);
        videoView = (VideoView) findViewById(R.id.videoPlayer);
        pd = ProgressDialog.show(this, "Cargando vídeo", "Espere...", true);
        Intent callingIntent = getIntent();
        String video = (String) callingIntent.getExtras().get("video");
        //String uriPath = "android.resource://"+getPackageName()+"/raw/"+video;
        String uriPath = "http://www.spoticoins.es/REEVideos/" + video + ".mp4";
        Uri uri = Uri.parse(uriPath);
        MediaController mc = new MediaController(this);
        mc.setAnchorView(videoView);
        try {
            videoView.setMediaController(mc);
            videoView.setVideoURI(uri);
            videoView.setOnPreparedListener(this);
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {

                    Log.e("Error", "No se puede visualizar el video");
                    pd.dismiss();
                   finish();
                    return true;
                }
            });
            videoView.start();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            pd.dismiss();
            e.printStackTrace();
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        pd.dismiss();


    }

}
