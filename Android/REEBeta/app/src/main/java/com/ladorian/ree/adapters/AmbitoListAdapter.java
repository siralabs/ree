package com.ladorian.ree.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.helpers.AmbitoItems;
import com.ladorian.ree.models.Ambito;
import com.ladorian.ree.models.Content;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GRANBAZU on 22/4/15.
 */
public class AmbitoListAdapter extends BaseAdapter {
    private Activity activity;
    private AmbitoItems ambitosList;
    private ArrayList<Content> data;
    private static LayoutInflater inflater = null;
    private Typeface miso_font;
    private int ambito;

    private static final String[][] iconTexts = {{"", "", "1", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "1.10", "1.11", "1.12", "1.13"},
            {"", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "17.1", "18", "19", "20"},
            {"", "21", "22", "23", "24", "25"},
            {"26", "27", "28", "29", "30", "31"}};
    private static final int[] iconBackgroundRes = {R.drawable.ambito1, R.drawable.ambito2, R.drawable.ambito3, R.drawable.ambito4};

    public AmbitoListAdapter(Activity a,int ambito) {
        this.activity = a;
        this.ambito = ambito;
        ambitosList = new AmbitoItems(activity,ambito);
        data = ambitosList.ambito.getContents();
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        miso_font = Typeface.createFromAsset(a.getAssets(),"fonts/miso.otf");
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Content getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.ambitos1_row, null);
        }
            Ambito amb = ambitosList.ambito;
            ImageView amb_bkg = (ImageView)convertView.findViewById(R.id.amb_bkg);
//        int bkg_id = activity.getResources().getIdentifier(amb.getBackground(),"drawable",activity.getPackageName());
        int bkg_id = activity.getResources().getIdentifier("s"+this.getItem(position).getIbkg(),"drawable",activity.getPackageName());
            amb_bkg.setImageResource(bkg_id);
            TextView title = (TextView)convertView.findViewById(R.id.title);
            title.setText(this.getItem(position).getTitle());
            title.setTypeface(miso_font);
            ImageView icon = (ImageView)convertView.findViewById(R.id.icon);
//            if(this.getItem(position).getIcon().equals("NO")) {
//                icon.setImageResource(R.drawable.no_icon);
//            } else {
//                int res_id = activity.getResources().getIdentifier("i"+this.getItem(position).getIcon(),"drawable",activity.getPackageName());
//                icon.setImageResource(res_id);
//            }
        icon.setImageResource(iconBackgroundRes[ambito]);
        ((TextView)convertView.findViewById(R.id.iconText)).setText(iconTexts[ambito][position]);

        return convertView;
    }

}
