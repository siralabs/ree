package com.ladorian.ree.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.models.Ambito;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by GRANBAZU on 24/4/15.
 */
public class ContentListAdapter extends BaseAdapter {

    private JSONArray contents;
    private StringBuilder textoInf;
    private Activity activity;
    private static LayoutInflater inflater = null;
    private Typeface miso_font;
    private String idioma;
    private String titulo;
    private String backgroundName;
    private String imageBackgroundName;

    public ContentListAdapter(String info, String title, JSONArray contents, Activity activity, String backgroundName) {
        this.contents = contents;
        this.activity = activity;
        this.backgroundName = backgroundName;
        this.imageBackgroundName = imageBackgroundName;
        SharedPreferences prefs = this.activity.getSharedPreferences("REE_PREFS", Context.MODE_PRIVATE);
        idioma = prefs.getString("idioma", null);
        titulo = title;
        if (info != null) {
            textoInf = new StringBuilder(info);
        }
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        miso_font = Typeface.createFromAsset(activity.getAssets(), "fonts/miso.otf");
    }

    public String getBackgroundName() {
        return this.backgroundName;
    }

    public String getImageBackgroundName() {
        return this.imageBackgroundName;
    }

    @Override
    public int getCount() {
        int cuenta;
        if (textoInf != null) {
            cuenta = contents.length() + 1;
        } else {
            cuenta = contents.length();
        }
        return cuenta;
    }

    @Override
    public JSONObject getItem(int position) {
        if (textoInf != null && position == 0) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("tipo", "INF");
                obj.put("archivo", textoInf.toString());
                obj.put("titulo", composeTitle(idioma));
                obj.put("titulo2", titulo);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return obj;
        } else {
            int newPos;
            if (textoInf != null) {
                newPos = position - 1;
            } else {
                newPos = position;
            }
            try {
                return contents.getJSONObject(newPos);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private String composeTitle(String idioma) {
        String titulo = "NARRACIÓN";
        if (idioma != null) {
            if (idioma.equals("castellano")) {
                titulo = "NARRACIÓN";
            } else if (idioma.equals("catala")) {
                titulo = "NARRACIÓ";
            } else {
                titulo = "NARRATION";
            }
        }
        return titulo;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contents_row, null);
        }
        TextView tipo = (TextView) convertView.findViewById(R.id.tipoTxt);
        TextView titulo = (TextView) convertView.findViewById(R.id.titulo);
        RelativeLayout tipoView = (RelativeLayout) convertView.findViewById(R.id.tipoView);
        tipoView.setBackgroundResource(getBackgroundButtonRes());
        if (textoInf != null && position == 0) {
            // tipo.setText("INF");
            tipo.setBackgroundResource(getBackgroundImageRes("INF"));
            //titulo.setText("NARRACIÓN");
            try {
                titulo.setText(getItem(0).getString("titulo"));
            } catch (JSONException e) {
                titulo.setText("NARRACIÓN");
            }

        } else {

            try {
                JSONObject item = this.getItem(position);
                //tipo.setText(item.getString("tipo"));
                tipo.setBackgroundResource(getBackgroundImageRes(item.getString("tipo")));


            } catch (JSONException e) {
                //tipo.setText("¿?");
                e.printStackTrace();
            }

            try {
                titulo.setText(this.getItem(position).getString("titulo"));
            } catch (JSONException e) {
                titulo.setText("");
                e.printStackTrace();
            }
        }
        titulo.setTypeface(miso_font);
        return convertView;
    }

    private int getBackgroundButtonRes() {
        int res_button;
        String resName = getBackgroundName() + "_oval_button";
        res_button = activity.getResources().getIdentifier(resName, "drawable", activity.getPackageName());
        return res_button;
    }

    private int getBackgroundImageRes(String type) {
        int resback_button;
        switch (type.toUpperCase()) {
            case "INF":
                resback_button = R.drawable.inf;
                break;
            case "PDF":
                resback_button = R.drawable.pdf;
                break;
            case "MP4":
                resback_button = R.drawable.video;
                break;
            default:
                resback_button = R.drawable.inf;
        }
        return resback_button;
    }
}
