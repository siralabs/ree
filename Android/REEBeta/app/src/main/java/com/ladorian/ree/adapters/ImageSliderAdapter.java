package com.ladorian.ree.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ladorian.ree.R;

/**
 * Created on 22/11/2015.
 */
public class ImageSliderAdapter extends PagerAdapter {
    Context context;
    private int[] GalImages = new int[]{
            R.drawable.portada1,
            R.drawable.portada2,
            R.drawable.portada3
    };
private int selectedItem;
    public int getSelectedITem(){
        return  selectedItem;
    }
    public ImageSliderAdapter(Context context) {
        this.context = context;
        selectedItem=1;
    }

    @Override
    public int getCount() {
        return GalImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        int padding =0;// context.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(GalImages[position]);
        ((ViewPager) container).addView(imageView, 0);
        selectedItem =position;
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}

