package com.ladorian.ree.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.helpers.Ambitos;
import com.ladorian.ree.models.Ambito;

import java.util.List;

/**
 * Created by GRANBAZU on 17/4/15.
 */
public class IntroductionListAdapter extends BaseAdapter {

    private Activity activity;
    private List<Ambito> data;
    private static LayoutInflater inflater = null;
    private Typeface miso_font;
    private SharedPreferences mPreferences;

    public IntroductionListAdapter(Activity a) {
        activity = a;
        mPreferences = a.getSharedPreferences("REE_PREFS",Context.MODE_PRIVATE);
        String idioma = mPreferences.getString("idioma",null);
        Ambitos mAmbitos;
        if(idioma!=null){
            if(idioma.equals("castellano")){
                mAmbitos = new Ambitos(a,"ES");
            } else if(idioma.equals("catala")){
                mAmbitos = new Ambitos(a,"CAT");
            } else {
                mAmbitos = new Ambitos(a,"EN");
            }
        } else {
            mAmbitos = new Ambitos(a,"ES");
        }
        data = mAmbitos.ambitos;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        miso_font = Typeface.createFromAsset(a.getAssets(),"fonts/miso.otf");
    }

    @Override
    public int getCount() {

        return data.size();
        //return 0;
    }

    @Override
    public Ambito getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null) {
            convertView = inflater.inflate(R.layout.ambitos_row_slim_shade, null);
            Ambito amb = getItem(position);
            TextView title = (TextView)convertView.findViewById(R.id.title);
            title.setText(amb.getTitle());
            title.setTypeface(miso_font);
            TextView titleIntro = (TextView)convertView.findViewById(R.id.titleIntro);
            titleIntro.setText(amb.getIntro());
            titleIntro.setTypeface(miso_font);
            ImageView icon = (ImageView)convertView.findViewById(R.id.icon);
            int res_id = activity.getResources().getIdentifier(amb.getIcon(),"drawable",activity.getPackageName());
            icon.setImageResource(res_id);
            ImageView amb_bkg = (ImageView)convertView.findViewById(R.id.amb_bkg);
            int bkg_id = activity.getResources().getIdentifier(amb.getBackground(),"drawable",activity.getPackageName());
            amb_bkg.setImageResource(bkg_id);
            ImageView amb_intro_bkg = (ImageView)convertView.findViewById(R.id.amb_bkg2);
            String darkColor = "dark_" + amb.getBackground();
            int bkg_intro_id = activity.getResources().getIdentifier(darkColor,"drawable",activity.getPackageName());
            amb_intro_bkg.setImageResource(bkg_intro_id);
            RelativeLayout layoutShade = (RelativeLayout) convertView.findViewById(R.id.layoutShade);
                layoutShade.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void reloadContent() {
        data = null;
        String idioma = mPreferences.getString("idioma",null);
        Ambitos mAmbitos;
        if(idioma!=null){
            if(idioma.equals("castellano")){
                mAmbitos = new Ambitos(activity,"ES");
            } else if(idioma.equals("catala")){
                mAmbitos = new Ambitos(activity,"CAT");
            } else {
                mAmbitos = new Ambitos(activity,"EN");
            }
        } else {
            mAmbitos = new Ambitos(activity,"ES");
        }

        data = mAmbitos.ambitos;
        notifyDataSetChanged();
    }
}
