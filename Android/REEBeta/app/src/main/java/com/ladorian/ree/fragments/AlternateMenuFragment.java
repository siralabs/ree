package com.ladorian.ree.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;

/**
 * Created on 24/10/2015.
 */
public class AlternateMenuFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {
    private Typeface miso_font;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).hideConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        ((MainActivity) getActivity()).setGoBack(true);
        SharedPreferences prefs = getActivity().getSharedPreferences("REE_PREFS", Context.MODE_PRIVATE);
        String idioma = prefs.getString("idioma", null);
        miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.config_fragment, container, false);
        RadioGroup rg = (RadioGroup) rootView.findViewById(R.id.selectLan);
        RadioButton b;
        if (idioma != null) {
            if (idioma.equals("castellano")) {
                b = (RadioButton) rootView.findViewById(R.id.castellano);

            } else if (idioma.equals("catala")) {
                b = (RadioButton) rootView.findViewById(R.id.catala);

            } else {
                b = (RadioButton) rootView.findViewById(R.id.english);
            }
            b.setChecked(true);
        } else {
            b = (RadioButton) rootView.findViewById(R.id.castellano);
            b.setChecked(true);
        }
        rg.setOnCheckedChangeListener(this);
        Button closeBtn = (Button) rootView.findViewById(R.id.closeConfig);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });
        TextView titleText = (TextView) rootView.findViewById(R.id.lanTitle);
        titleText.setTypeface(miso_font);
        return rootView;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        SharedPreferences.Editor editor = getActivity().getSharedPreferences("REE_PREFS",Context.MODE_PRIVATE).edit();
        if (checkedId == R.id.castellano) {
            editor.putString("idioma", "castellano");
        } else if (checkedId == R.id.catala) {
            editor.putString("idioma", "catala");
        } else if (checkedId == R.id.english) {
            editor.putString("idioma", "english");
        }
        editor.apply();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).hideConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        ((MainActivity) getActivity()).setGoBack(true);
    }
}

