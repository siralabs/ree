package com.ladorian.ree.fragments;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.adapters.AmbitoListAdapter;
import com.ladorian.ree.helpers.AmbitoItems;
import com.ladorian.ree.helpers.PlaybackService;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

/**
 * Created on 24/10/2015.
 */
public class AmbitoFragment extends Fragment implements AdapterView.OnItemClickListener {
    public int nAmb;
    private AmbitoItems ambito;
    private AmbitoListAdapter adapter;

    public AmbitoFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        ((MainActivity) getActivity()).setGoBack(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setGoBack(true);
        ambito = new AmbitoItems(getActivity(), nAmb);
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//        ImageView bottomView = (ImageView) rootView.findViewById(R.id.bottom);
//        int res_id = getActivity().getResources().getIdentifier(ambito.ambito.getBottom(), "drawable", getActivity().getPackageName());
//        bottomView.setImageResource(res_id);
        ListView list = (ListView) rootView.findViewById(R.id.list);
        adapter = new AmbitoListAdapter(getActivity(), nAmb);
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(list);
        list.setAdapter(animationAdapter);
        list.setOnItemClickListener(this);
        return rootView;
    }

    public void setnAmb(int nAmb) {
        this.nAmb = nAmb;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (nAmb == 3 && position == ambito.ambito.getContents().size() - 1) {
//				MediaPlayer mp = ((MainActivity) getActivity()).mMediaPlayer;
            MediaPlayer mp = PlaybackService.getMediaPlayer();
//				if (mp != null && mp.isPlaying())
            if (PlaybackService.isPlaying()) {
                mp.stop();
            }
            Context context = getActivity().getApplicationContext();
            Intent intent = null;
            intent = new Intent(context, CuestionarioActivity.class);
            intent.putExtra("idioma", getActivity().getSharedPreferences("REE_PREFS",Context.MODE_PRIVATE).getString("idioma", null));
            startActivity(intent);
        } else {
            ((MainActivity) this.getActivity()).loadContents(ambito, position);
        }

    }
}
