package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.activities.PDFactivity;
import com.ladorian.ree.activities.TextActivity;
import com.ladorian.ree.activities.VideoPlayerActivity;
import com.ladorian.ree.adapters.ContentListAdapter;
import com.ladorian.ree.helpers.AmbitoItems;
import com.ladorian.ree.helpers.PlaybackService;
import com.ladorian.ree.helpers.Utilities;
import com.ladorian.ree.models.Ambito;
import com.ladorian.ree.models.Content;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created on 24/10/2015.
 */
public class ContentsFragment extends Fragment implements AdapterView.OnItemClickListener, SeekBar.OnSeekBarChangeListener {
    private static SeekBar mSeekBar;
    private AmbitoItems ambito;
    private int position;
    private Typeface miso_font;
    private ContentListAdapter mAdapter;
    private Handler mHandler = new Handler();
    private Utilities utils;
    private String audioName;
    private ImageButton playBtn;
    private static boolean sGoingToTextScreen;

    public static boolean isGoingToNextScreen() {
        return sGoingToTextScreen;
    }

    @Override
    public void onResume() {
        super.onResume();
        sGoingToTextScreen = false;
        if (!PlaybackService.isPlaying()) {
            onStopTrackingTouch(mSeekBar);
        }
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        updatePlayBtn();
    }

    @Override
    public void onPause() {
        if (!ContentsFragment.isGoingToNextScreen()) {
            ((MainActivity) getActivity()).sendPlaybackIntent(PlaybackService.ACTION_PAUSE);
        }

        super.onPause();
    }

    @Override
    public void onDestroy() {
        ((MainActivity) getActivity()).sendPlaybackIntent(PlaybackService.ACTION_STOP);
        mHandler.removeCallbacks((mUpdateTimeTask));
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setGoBack(true);
        ((MainActivity) getActivity()).showAllBar();
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        utils = new Utilities();
        miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.content_fragment, container, false);
        Ambito amb = ambito.ambito;
        Content apartado = amb.getContents().get(position);
        LinearLayout audioPlayer = (LinearLayout) rootView.findViewById(R.id.audioPlayer);
        this.audioName = apartado.getAudio();
        if (audioName.equals("NO")) {
            audioPlayer.setVisibility(View.GONE);
        }
        //this.audioName = "a_002_identificacion_de_la_muestra";
//        Button bottom = (Button) rootView.findViewById(R.id.play_btn);
//        int res_id = getActivity().getResources().getIdentifier("ambito1_oval_button", "drawable", getActivity().getPackageName());
//        bottom.setBackgroundResource(res_id);
//        TextView title = (TextView) rootView.findViewById(R.id.ambito);
//        title.setText(amb.getTitle());
//        title.setTypeface(miso_font);
        int titleColor;
        TextView subtitle = (TextView) rootView.findViewById(R.id.title);
        subtitle.setText(apartado.getTitle());
        subtitle.setTypeface(miso_font);
        ImageView subTitleBackground = (ImageView) rootView.findViewById(R.id.amb_bkg);


        int backgroundPlayRes;
        if (amb.getBackground().equals("ambito1")) {
            //   titleColor = getActivity().getResources().getColor(R.color.ambito1);
            subTitleBackground.setImageResource(R.drawable.ambito1);
            backgroundPlayRes = R.drawable.ambito1_oval_button;

        } else if (amb.getBackground().equals("ambito2")) {
            //  titleColor = getActivity().getResources().getColor(R.color.ambito2);
            subTitleBackground.setImageResource(R.drawable.ambito2);
            backgroundPlayRes = R.drawable.ambito2_oval_button;
        } else if (amb.getBackground().equals("ambito3")) {
            // titleColor = getActivity().getResources().getColor(R.color.ambito3);
            subTitleBackground.setImageResource(R.drawable.ambito3);
            backgroundPlayRes = R.drawable.ambito3_oval_button;
        } else {
            // titleColor = getActivity().getResources().getColor(R.color.ambito4);
            subTitleBackground.setImageResource(R.drawable.ambito4);
            backgroundPlayRes = R.drawable.ambito4_oval_button;
        }
        //      title.setTextColor(titleColor);
        ImageView image_contentBackground = (ImageView) rootView.findViewById(R.id.img_ambito);
        image_contentBackground.setImageResource(getImageBackgroundButtonRes(apartado.getIbkg()+"_h",getActivity()));
        if (apartado.getText().equals("NO")) {
            mAdapter = new ContentListAdapter(null, null, apartado.getContents(), getActivity(), amb.getBackground());
        } else {
            mAdapter = new ContentListAdapter(apartado.getText(), apartado.getTitle(), apartado.getContents(), getActivity(), amb.getBackground());
        }
        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
        playBtn = (ImageButton) rootView.findViewById(R.id.play_audio2);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).useAudio(audioName);
                updatePlayBtn();
            }
        });
        playBtn.setBackgroundResource(backgroundPlayRes);
        updatePlayBtn();
        mSeekBar = (SeekBar) rootView.findViewById(R.id.audioSeek);
        mSeekBar.setOnSeekBarChangeListener(this);
        updateProgressBar();
        return rootView;
    }
    private int getImageBackgroundButtonRes(String ibgkName, Activity activity){
        int res_button;
        String resName = ibgkName;
        res_button = activity.getResources().getIdentifier(resName, "drawable", activity.getPackageName());
        return res_button;
    }
    public void setAmbito(AmbitoItems ambito) {
        this.ambito = ambito;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        JSONObject item = mAdapter.getItem(position);
        try {
            if (item.getString("tipo").equals("mp4")) {
//					MediaPlayer mp = ((MainActivity) getActivity()).mMediaPlayer;
                MediaPlayer mp = PlaybackService.getMediaPlayer();
//					if (mp != null && mp.isPlaying())
                if (PlaybackService.isPlaying()) {
                    mp.pause();
                }
                Context context = getActivity().getApplicationContext();
                Intent intent = null;
                intent = new Intent(context, VideoPlayerActivity.class);
                intent.putExtra("video", item.getString("archivo"));
                startActivity(intent);
            } else if (item.getString("tipo").equals("PDF")) {
                Context context = getActivity().getApplicationContext();
                Intent intent = null;
                intent = new Intent(context, PDFactivity.class);
                intent.putExtra("pdf", item.getString("archivo") + ".pdf");
                startActivity(intent);
            } else if (item.getString("tipo").equals("TXT") || item.getString("tipo").equals("INF")) {
                sGoingToTextScreen = true;
                Context context = getActivity().getApplicationContext();
                Intent intent = null;
                intent = new Intent(context, TextActivity.class);
                intent.putExtra("texto", item.getString("archivo"));
                switch (item.getString("tipo")) {
                    case "TXT":
                        intent.putExtra("titulo2", item.getString("titulo"));
                        break;
                    case "INF":
                        intent.putExtra("titulo2", item.getString("titulo2"));
                        break;
                }


                startActivity(intent);
            } else if (item.getString("tipo").equals("WEB")) {
                String url = item.getString("archivo");
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ///SEEKBARMETHODS

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
//			MediaPlayer mp = ((MainActivity) getActivity()).mMediaPlayer;
        MediaPlayer mp = PlaybackService.getMediaPlayer();
        if (mp != null) {
            mHandler.removeCallbacks(mUpdateTimeTask);
            int totalDuration = mp.getDuration();
            int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

            // forward or backward to certain seconds
            mp.seekTo(currentPosition);

            // update timer progress again
            updateProgressBar();
        } else {
            mSeekBar.setProgress(0);
        }
    }

    private void updatePlayBtn() {
//			MediaPlayer mp = ((MainActivity) getActivity()).mMediaPlayer;
        MediaPlayer mp = PlaybackService.getMediaPlayer();
//			if (mp != null && mp.isPlaying()
        if (PlaybackService.isPlaying() && ((MainActivity) getActivity()).getCurrentAudio().equals(audioName)) {
            playBtn.setImageResource(R.drawable.pause_btn);
        } else {
            playBtn.setImageResource(R.drawable.play_btn);
        }
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        @Override
        public void run() {
//				MediaPlayer mp = ((MainActivity) getActivity()).mMediaPlayer;
            MediaPlayer mp = PlaybackService.getMediaPlayer();
//				if (mp != null && mp.isPlaying()
            if (PlaybackService.isPlaying() && ((MainActivity) getActivity()).getCurrentAudio().equals(audioName)) {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();
                mSeekBar.setProgress(utils.getProgressPercentage(currentDuration, totalDuration));
            }
            mHandler.postDelayed(this, 100);
        }
    };
}

