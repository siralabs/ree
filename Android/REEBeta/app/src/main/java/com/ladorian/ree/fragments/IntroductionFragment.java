package com.ladorian.ree.fragments;

/**
 * Created on 24/10/2015.
 */

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.adapters.IntroductionListAdapter;
import com.ladorian.ree.helpers.Traducciones;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class IntroductionFragment extends Fragment implements AbsListView.OnScrollListener, AdapterView.OnItemClickListener {

    public IntroductionListAdapter mAdapter;
    private ListView list;
    private SharedPreferences mPreferences;
    TextView bienvinidoIntro;

    public IntroductionFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        configuraNavegacion(true);
        mAdapter.reloadContent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MainActivity mainActivity = configuraNavegacion(true);
        View rootView = inflater.inflate(R.layout.introduccion_main, container, false);
        traducirFragment(mainActivity, rootView);


        mAdapter = new IntroductionListAdapter(mainActivity);
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(mAdapter);
        list = (ListView) rootView.findViewById(R.id.list);
        animationAdapter.setAbsListView(list);
        list.setAdapter(animationAdapter);
        list.setOnScrollListener(this);
        list.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.i("REE", "Visible item " + firstVisibleItem);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //list.smoothScrollToPosition(position);
        //parent.getItemAtPosition(position);
        int sizeVariation = 0;
        int baseSize = getResources().getDimensionPixelSize(R.dimen.intro_text_text_size);
//        for (int partialID = 0; partialID < 4; partialID++) {
//            View partialView = parent.getChildAt(partialID);
//            if (partialView != null) {
//                RelativeLayout layoutShade = (RelativeLayout) partialView.findViewById(R.id.layoutShade);
//                if (layoutShade != null) {
//                    int visibility =  layoutShade.getVisibility();
//                    if (visibility != View.GONE) {
//                        layoutShade.setVisibility(View.GONE);
//                        sizeVariation = 0;
//                    } else {
//                        if (partialID == position) {
//                            layoutShade.setVisibility(View.VISIBLE);
//                            sizeVariation = -10;
//                        }
//                    }
//                }
//            }
//        }
        int partialActiveID =-1;
        for (int partialID = 0; partialID < 4; partialID++) {
            View partialView = parent.getChildAt(partialID);
            if (partialView != null) {
                RelativeLayout layoutShade = (RelativeLayout) partialView.findViewById(R.id.layoutShade);
                if (layoutShade != null) {
                    int visibility = layoutShade.getVisibility();
                    if(visibility == View.GONE && position == partialID){
                        partialActiveID = partialID;
                    }
                 //   if (visibility != View.GONE) {
                        layoutShade.setVisibility(View.GONE);
                        sizeVariation = 0;
                 //   }
                }
            }
        }
            View partialView2 = parent.getChildAt(position);
            if (partialView2 != null && partialActiveID == position) {
                RelativeLayout layoutShade = (RelativeLayout) partialView2.findViewById(R.id.layoutShade);
                if (layoutShade != null) {
                    int visibility = layoutShade.getVisibility();
                    if (visibility == View.GONE) {
                        layoutShade.setVisibility(View.VISIBLE);
                        sizeVariation = -10;
                    }

                }

            }
            if (bienvinidoIntro != null) {
                bienvinidoIntro.setTextSize(baseSize + sizeVariation);
            }
//        RelativeLayout layoutShade = (RelativeLayout) view.findViewById(R.id.layoutShade);
//
//        if (layoutShade.getVisibility() != View.GONE) {
//            layoutShade.setVisibility(View.GONE);
//        } else {
//            layoutShade.setVisibility(View.VISIBLE);
//        }

            //   ((MainActivity) this.getActivity()).changeFragment(position);
        }

    private MainActivity configuraNavegacion(boolean back) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.showAllBar();
        mainActivity.hideGuideBtn();
        mainActivity.showConfigBtn();
        mainActivity.setGoBack(back);
        return mainActivity;
    }

    protected void traducirFragment(Activity activity, View view) {
        Traducciones trad = new Traducciones(activity);

        Typeface miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");

        TextView bienvenidoTxt = (TextView) view.findViewById(R.id.bienvenido);
        bienvenidoTxt.setTypeface(miso_font);
        bienvenidoTxt.setText(trad.getTexto("Bienvenido"));

        bienvinidoIntro = (TextView) view.findViewById(R.id.bienvenidoIntro);
        bienvinidoIntro.setTypeface(miso_font);
        bienvinidoIntro.setText(trad.getTexto("BienvenidoIntro"));


    }
}

