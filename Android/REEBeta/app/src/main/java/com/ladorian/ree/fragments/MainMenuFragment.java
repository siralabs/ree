package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.AudioGuideActivity;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.adapters.ImageSliderAdapter;
import com.ladorian.ree.helpers.Traducciones;

/**
 * Created on 24/10/2015.
 */
public class MainMenuFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity mainActivity = configuraNavegacion(true);

        View rootView = inflater.inflate(R.layout.mainmenu_fragment, container, false);

        configurarBotones(mainActivity, rootView);
        traducirFragment(mainActivity, rootView);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        configuraNavegacion(true);
    }

    private MainActivity configuraNavegacion(boolean back) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.showAllBar();
        mainActivity.hideConfigBtn();
        mainActivity.hideGuideBtn();
        mainActivity.setGoBack(back);
        return mainActivity;
    }

    @Override
    public void onClick(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        switch (view.getId()) {
            case R.id.introduccion_btn:
                mainActivity.changeFragment(new IntroductionFragment());
                break;
            case R.id.audioguia_btn:
                mainActivity.changeFragment(new PlaceholderFragment());
                break;
            case R.id.prepararvisita_btn:
                mainActivity.changeFragment(new PreparaFragment());
                break;
            case R.id.castellano:
                mainActivity.changeLanguage("castellano", this);
                break;
            case R.id.catala:
                mainActivity.changeLanguage("catala", this);
                break;
            case R.id.english:
                mainActivity.changeLanguage("english", this);
                break;
            case R.id.imgsliderportada:
                mainActivity.changeFragment(new IntroductionFragment());
break;
        }
    }

    protected void configurarBotones(final MainActivity activity, View view) {
        Button introduccionBtn = (Button) view.findViewById(R.id.introduccion_btn);
        introduccionBtn.setOnClickListener(this);

        Button audioguiaBtn = (Button) view.findViewById(R.id.audioguia_btn);
        audioguiaBtn.setOnClickListener(this);

        Button prepararvisitaBtn = (Button) view.findViewById(R.id.prepararvisita_btn);
        prepararvisitaBtn.setOnClickListener(this);

        Button castellanoBtn = (Button) view.findViewById(R.id.castellano);
        castellanoBtn.setOnClickListener(this);

        Button catalaBtn = (Button) view.findViewById(R.id.catala);
        catalaBtn.setOnClickListener(this);

        Button englishBtn = (Button) view.findViewById(R.id.english);
        englishBtn.setOnClickListener(this);

        Button audioGuiaBtn = (Button) view.findViewById(R.id.audioguia);
        audioGuiaBtn.setOnClickListener(this);
        final Context c = activity;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            audioGuiaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(c, AudioGuideActivity.class);
                    startActivity(i);
                }
            });
        } else {
            audioGuiaBtn.setVisibility(View.GONE);
        }

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.imgsliderportada);
        final ImageSliderAdapter adapter = new ImageSliderAdapter(activity.getBaseContext());
        viewPager.setAdapter(adapter);


        // somewhere where you setup your viewPager add this
        viewPager.setOnTouchListener(
                new View.OnTouchListener() {
                    private boolean moved;

                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            moved = false;
                        }
                        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                            moved = true;
                        }
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            if (!moved) {
                                view.performClick();
                            }
                        }

                        return false;
                    }
                }
        );

// then you can simply use the standard onClickListener ...
        viewPager.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        switch (viewPager.getCurrentItem()){
                            case 0:
                                activity.changeFragment(new IntroductionFragment());
                                break;
                            case 1:
                                activity.changeFragment(new PlaceholderFragment());
                                break;
                            case 2:
                                activity.changeFragment(new PreparaFragment());
                                break;
                        }

                    }
                }
        );

    }

    protected void traducirFragment(Activity activity, View view) {
        Traducciones trad = new Traducciones(activity);

        Typeface miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");

        Button introduccionBtn = (Button) view.findViewById(R.id.introduccion_btn);
        introduccionBtn.setTypeface(miso_font);
        introduccionBtn.setText(trad.getTexto("Introduccion"));

        Button audioguiaBtn = (Button) view.findViewById(R.id.audioguia_btn);
        audioguiaBtn.setTypeface(miso_font);
        audioguiaBtn.setText(trad.getTexto("Comenzar visita (audioguia)"));

        Button prepararvisitaBtn = (Button) view.findViewById(R.id.prepararvisita_btn);
        prepararvisitaBtn.setTypeface(miso_font);
        prepararvisitaBtn.setText(trad.getTexto("Prepara tu visita"));

        Button castellanoBtn = (Button) view.findViewById(R.id.castellano);
        castellanoBtn.setOnClickListener(this);

        Button catalaBtn = (Button) view.findViewById(R.id.catala);
        catalaBtn.setOnClickListener(this);

        Button englishBtn = (Button) view.findViewById(R.id.english);
        englishBtn.setOnClickListener(this);
    }
}
