package com.ladorian.ree.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.AudioGuideActivity;

/**
 * Created by GRANBAZU on 21/5/15.
 */

public class NotificationFragment extends Fragment implements Button.OnClickListener {
    private boolean isMulti;
    private String title;
    private boolean isFirst;
    private boolean isLast;
    private int ambito;
    private boolean isClicked = false;

    public static final NotificationFragment newInstance(boolean multi,String title,boolean isFirst,boolean isLast,int ambito) {
        NotificationFragment f = new NotificationFragment();
        Bundle bdl = new Bundle(2);
        bdl.putBoolean("isMulti", multi);
        bdl.putString("title", title);
        bdl.putBoolean("isFirst", isFirst);
        bdl.putBoolean("isLast", isLast);
        bdl.putInt("ambito",ambito);
        f.setArguments(bdl);

        return  f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isMulti = getArguments().getBoolean("isMulti");
        title = getArguments().getString("title");
        isFirst = getArguments().getBoolean("isFirst");
        isLast = getArguments().getBoolean("isLast");
        ambito = getArguments().getInt("ambito");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_fragment,container,false);
        TextView titleView = (TextView)rootView.findViewById(R.id.contentTitle);
        titleView.setText(this.title);
        View backView = (View)rootView.findViewById(R.id.notificationBKG);
        switch (ambito) {
            case 0:
                backView.setBackgroundColor(getResources().getColor(R.color.ambito1));
                break;
            case 1:
                backView.setBackgroundColor(getResources().getColor(R.color.ambito2));
                break;
            case 2:
                backView.setBackgroundColor(getResources().getColor(R.color.ambito3));
                break;
            case 3:
                backView.setBackgroundColor(getResources().getColor(R.color.ambito4));
                break;
            default:
                backView.setBackgroundColor(getResources().getColor(R.color.ambito1));
                break;
        }
        Button loadBtn = (Button)rootView.findViewById(R.id.loadBtn);
        loadBtn.setOnClickListener(this);
        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        Button nextBtn = (Button)rootView.findViewById(R.id.nextBtn);
        Button prevBtn = (Button)rootView.findViewById(R.id.prevBtn);
        if(!isMulti) {
            nextBtn.setVisibility(View.GONE);
            prevBtn.setVisibility(View.GONE);
            cancelBtn.setVisibility(View.GONE);
        } else {
            if(isFirst && !isLast){
                prevBtn.setVisibility(View.GONE);
                nextBtn.setVisibility(View.VISIBLE);
            }
            else if(isLast && !isFirst) {
                nextBtn.setVisibility(View.GONE);
                prevBtn.setVisibility(View.VISIBLE);
            }
            cancelBtn.setVisibility(View.GONE);
        }
        /*nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isClicked) {
                    ((AudioGuideActivity) getActivity()).loadNextNotification();
                }
                isClicked = true;
            }
        });
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isClicked) {
                    ((AudioGuideActivity) getActivity()).loadPrevNotification();
                }
                isClicked = true;
            }
        });*/
        nextBtn.setOnClickListener(this);
        prevBtn.setOnClickListener(this);
        TextView firstLabel = (TextView)rootView.findViewById(R.id.firstLabel);
        SharedPreferences prefs = getActivity().getSharedPreferences("REE_PREFS",Context.MODE_PRIVATE);
        String idioma = prefs.getString("idioma",null);
        if(idioma!=null) {
            if(idioma.equals("castellano")) {
                nextBtn.setText("Siguiente");
                prevBtn.setText("Anterior");
                loadBtn.setText("Ir");
                firstLabel.setText("Te encuentras en...");
            } else if(idioma.equals("catala")){
                nextBtn.setText("Següent");
                prevBtn.setText("Anterior");
                loadBtn.setText("Carregar");
                firstLabel.setText("Et trobes en...");

            } else {
                nextBtn.setText("Next");
                prevBtn.setText("Previous");
                loadBtn.setText("Load");
                firstLabel.setText("You are at...");
            }

        } else {
            nextBtn.setText("Siguiente");
            prevBtn.setText("Anterior");
            loadBtn.setText("Ir");
            firstLabel.setText("Te encuentras en...");
        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        View rootView = this.getView();
        assert rootView != null;
        if(!isClicked) {
            if (v.equals(rootView.findViewById(R.id.nextBtn))) {
                ((AudioGuideActivity) getActivity()).loadNextNotification();
            } else if (v.equals(rootView.findViewById(R.id.prevBtn))) {
                ((AudioGuideActivity) getActivity()).loadPrevNotification();
            } else if(v.equals(rootView.findViewById(R.id.loadBtn))) {
                if (!isMulti) {
                    ((AudioGuideActivity) getActivity()).loadNextBeaconData();
                } else {
                    ((AudioGuideActivity) getActivity()).loadNextMultiContent();
                }
            }
        }
        isClicked = true;
    }
}
