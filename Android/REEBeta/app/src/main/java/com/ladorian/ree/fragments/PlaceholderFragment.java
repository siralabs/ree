package com.ladorian.ree.fragments;

/**
 * Created on 24/10/2015.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.adapters.MainListAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment implements AbsListView.OnScrollListener, AdapterView.OnItemClickListener {

    public MainListAdapter mAdapter;
    private ListView list;
    private SharedPreferences mPreferences;

    public PlaceholderFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        ((MainActivity) getActivity()).setGoBack(true);
        mAdapter.reloadContent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainActivity) getActivity()).showAllBar();
        ((MainActivity) getActivity()).showConfigBtn();
        ((MainActivity) getActivity()).hideGuideBtn();
        ((MainActivity) getActivity()).setGoBack(true);
        mAdapter = new MainListAdapter(this.getActivity());
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(mAdapter);
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        list = (ListView) rootView.findViewById(R.id.list);
        animationAdapter.setAbsListView(list);
        list.setAdapter(animationAdapter);
        list.setOnScrollListener(this);
        list.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.i("REE", "Visible item " + firstVisibleItem);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //list.smoothScrollToPosition(position);
        ((MainActivity) this.getActivity()).changeFragment(position);
    }
}

