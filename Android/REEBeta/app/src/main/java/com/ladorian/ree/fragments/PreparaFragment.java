package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.helpers.Traducciones;

/**
 * Created on 24/10/2015.
 */
public class PreparaFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity mainActivity = configuraNavegacion(true);

        View rootView = inflater.inflate(R.layout.prepara_fragment, container, false);

        traducirFragment(mainActivity,rootView);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        configuraNavegacion(true);
    }

    private MainActivity configuraNavegacion(boolean back){
        MainActivity mainActivity = (MainActivity)getActivity();
        mainActivity.showAllBar();
        mainActivity.showConfigBtn();
        mainActivity.hideGuideBtn();
        mainActivity.setGoBack(back);
        return mainActivity;
    }

    protected void traducirFragment(Activity activity, View view)
    {
        Traducciones trad = new Traducciones(activity);

        Typeface miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");

        Button horariosBtn = (Button) view.findViewById(R.id.horarios);
        horariosBtn.setTypeface(miso_font);
        horariosBtn.setText(trad.getTexto("Horarios"));

        Button comollegarBtn = (Button) view.findViewById(R.id.comollegar);
        comollegarBtn.setTypeface(miso_font);
        comollegarBtn.setText(trad.getTexto("ComoLlegar"));

        Button gruposyvisitasBtn = (Button) view.findViewById(R.id.gruposyvisitas);
        gruposyvisitasBtn.setTypeface(miso_font);
        gruposyvisitasBtn.setText(trad.getTexto("Gruposyvisitas"));

        Button materialdidacticoBtn = (Button) view.findViewById(R.id.materialdidactico);
        materialdidacticoBtn.setTypeface(miso_font);
        materialdidacticoBtn.setText(trad.getTexto("Materialdidactico"));

        TextView prepara_text = (TextView) view.findViewById(R.id.prepara_text);
//        prepara_text.setTypeface(miso_font);
//        prepara_text.setText(trad.getTexto("Prepara tu visita"));

        TextView prepara_long_text = (TextView) view.findViewById(R.id.prepara_long_text);
        prepara_long_text.setTypeface(miso_font);
        prepara_long_text.setText(trad.getTexto("Prepara_long_text"));
    }


}