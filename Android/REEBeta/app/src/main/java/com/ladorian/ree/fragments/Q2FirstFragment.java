package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.models.Cuestionario;

public class Q2FirstFragment extends Fragment implements TextView.OnEditorActionListener {
    private static Typeface miso_font;
    private EditText soy_edit;
    private EditText conoci_edit;
    private Traducciones trad;
    private String sexo;
    private int edad;
    private String soy;
    private String conoci;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final CuestionarioActivity activity = (CuestionarioActivity) getActivity();
        View rootView = inflater.inflate(R.layout.q2_first_fragment, container, false);
        traducirFragment(activity, rootView);

        colocarSituacionAnterior(activity, rootView);

        final Q2FirstFragment weakSelf = this;
        configurarEventos(activity, rootView);
        configurarNavegacion(activity, rootView);


        return rootView;

    }

    private void configurarEventos(final CuestionarioActivity activity, final View view) {
        RadioGroup radioSoy = (RadioGroup) view.findViewById(R.id.radioSoy);
        radioSoy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                EditText editSoy = (EditText) view.findViewById(R.id.soy_edit);
                switch (checkedId) {
                    case R.id.radioEstudiante:
                    case R.id.radioTurista:
                    case R.id.radioProfesor:
                        editSoy.setVisibility(View.INVISIBLE);
                        editSoy.setText("");
                        break;
                    case R.id.radioOtro:
                        editSoy.setVisibility(View.VISIBLE);

                        break;
                }
            }
        });
        RadioGroup radioConoci = (RadioGroup) view.findViewById(R.id.radioConoci);
        radioConoci.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                EditText conociEdit = (EditText) view.findViewById(R.id.conoci_edit);
                switch (checkedId) {
                    case R.id.radioInternet:
                    case R.id.radioMediosComunicacion:
                    case R.id.radioVisitaGrupo:
                        conociEdit.setVisibility(View.INVISIBLE);
                        conociEdit.setText("");
                        break;
                    case R.id.radioOtroConoci:
                        conociEdit.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
        final EditText myEditText = (EditText) view.findViewById(R.id.edadValue);
        final int maxTextLength = 2;//max length of your text

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(maxTextLength);
        myEditText.setFilters(filterArray);

        myEditText.addTextChangedListener(new TextWatcher() {


            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence txtWatcherStr, int start, int before, int count) {
                if (count >= maxTextLength) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                  //  imm.hideSoftInputFromWindow(myEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
            }
        });
    }

    private void configurarNavegacion(final CuestionarioActivity activity, final View view) {


        ImageButton next_btn = (ImageButton) view.findViewById(R.id.next_btn);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAnswers(activity, view)) {
                    activity.getmCuestionario().setSoy(soy);
                    activity.getmCuestionario().setConoci(conoci);
                    activity.getmCuestionario().setEdad(edad);
                    activity.getmCuestionario().setSexo(sexo);
                    activity.changeQuestion(1);
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void colocarSituacionAnterior(CuestionarioActivity activity, View view) {
        Cuestionario cuestionario = activity.getmCuestionario();

        EditText edadEdit = (EditText) view.findViewById(R.id.edadValue);
        if (cuestionario.getEdad() != 0) {
            edadEdit.setText(String.valueOf(cuestionario.getEdad()));
        }

        switch (cuestionario.getSexo()) {
            case "M":
                setRadioIndexSelected(activity, view, R.id.radioSex, R.id.radioMale);
                break;
            case "F":
                setRadioIndexSelected(activity, view, R.id.radioSex, R.id.radioFemale);
                break;
            default:
                break;
        }

        switch (cuestionario.getSoy()) {
            case "Estudiante":
                setRadioIndexSelected(activity, view, R.id.radioSoy, R.id.radioEstudiante);
                break;
            case "Profesor":
                setRadioIndexSelected(activity, view, R.id.radioSoy, R.id.radioProfesor);
                break;
            case "Turista":
                setRadioIndexSelected(activity, view, R.id.radioSoy, R.id.radioTurista);

                break;
            case "":
                break;
            default:
                setRadioIndexSelected(activity, view, R.id.radioSoy, R.id.radioOtro);
                EditText soyEdit = (EditText) view.findViewById(R.id.soy_edit);
                soyEdit.setVisibility(View.VISIBLE);
                soyEdit.setText(cuestionario.getSoy());
                break;
        }


        switch (cuestionario.getConoci()) {
            case "Internet":
                setRadioIndexSelected(activity, view, R.id.radioConoci, R.id.radioInternet);
                break;
            case "Medios Comunicacion":
                setRadioIndexSelected(activity, view, R.id.radioConoci, R.id.radioMediosComunicacion);
                break;
            case "Visita en grupo":
                setRadioIndexSelected(activity, view, R.id.radioConoci, R.id.radioVisitaGrupo);
                break;
            case "":
                break;
            default:
                setRadioIndexSelected(activity, view, R.id.radioConoci, R.id.radioOtroConoci);
                EditText conociEdit = (EditText) view.findViewById(R.id.conoci_edit);
                conociEdit.setVisibility(View.VISIBLE);
                conociEdit.setText(cuestionario.getConoci());
                break;
        }
    }

    private void setRadioIndexSelected(final CuestionarioActivity activity, final View view, int idRadioGroup, int idRadioButton) {
        RadioGroup radioButtonGroup = (RadioGroup) view.findViewById(idRadioGroup);
        radioButtonGroup.check(idRadioButton);
    }

    private int getRadioIndexSelected(final CuestionarioActivity activity, final View view, int idRadioGroup) {
        RadioGroup radioButtonGroup = (RadioGroup) view.findViewById(idRadioGroup);

        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int idx = radioButtonGroup.indexOfChild(radioButton);
        return idx;
    }

    private Boolean checkAnswers(final CuestionarioActivity activity, final View view) {
        Boolean checked = true;
        EditText edadText = (EditText) view.findViewById(R.id.edadValue);
        if (edadText.getText() != null && !edadText.getText().toString().trim().equals("")) {
            edad = Integer.parseInt(edadText.getText().toString().trim());
        }
        int indexSexo = getRadioIndexSelected(activity, view, R.id.radioSex);

        switch (indexSexo) {
            case 0:
                sexo = "M";
                break;

            case 1:
                sexo = "F";
                break;

            default:
                sexo = "";
        }
        int indexSoy = getRadioIndexSelected(activity, view, R.id.radioSoy);

        switch (indexSoy) {
            case 0:
                soy = "Estudiante";
                break;
            case 1:
                soy = "Profesor";
                break;
            case 2:
                soy = "Turista";
                break;
            case 3:
                soy = soy_edit.getText().toString();
                break;
            default:
                soy = "";
                checked = false;
                break;
        }
        int indexConoci = getRadioIndexSelected(activity, view, R.id.radioConoci);

        switch (indexConoci) {
            case 0:
                conoci = "Internet";
                break;
            case 1:
                conoci = "Medios Comunicacion";
                break;
            case 2:
                conoci = "Visita en grupo";
                break;
            case 3:
                conoci = conoci_edit.getText().toString();
                break;
            default:
                conoci = "";
                checked = false;
                break;
        }
        return checked;
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            v.clearFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;
    }

    protected void traducirFragment(Activity activity, View view) {
        trad = new Traducciones(activity);
        miso_font = Typeface.createFromAsset(activity.getAssets(), "fonts/miso.otf");
        TextView soyTxt = (TextView) view.findViewById(R.id.soyTexto);
        soyTxt.setTypeface(miso_font);
        soyTxt.setText(trad.getTexto("soy"));
        TextView conociTxt = (TextView) view.findViewById(R.id.conociTexto);
        conociTxt.setText(trad.getTexto("conoci"));
        conociTxt.setTypeface(miso_font);
        TextView edadTxt = (TextView) view.findViewById(R.id.edadTexto);
        edadTxt.setText(trad.getTexto("edad"));
//        edadTxt.setHint(trad.getTexto("Ej. 14"));
        edadTxt.setTypeface(miso_font);
        TextView sexoTxt = (TextView) view.findViewById(R.id.sexoTexto);
        sexoTxt.setText(trad.getTexto("sexo"));
        sexoTxt.setTypeface(miso_font);
        ImageButton next_btn = (ImageButton) view.findViewById(R.id.next_btn);
        switch (trad.getIdioma()) {
            case "castellano":
                next_btn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                next_btn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                next_btn.setImageResource(R.drawable.next_arrow_en);
                break;
        }
        RadioButton radioMale = (RadioButton) view.findViewById(R.id.radioMale);
        radioMale.setText(trad.getTexto("Hombre"));
        radioMale.setTypeface(miso_font);
        RadioButton radioFemale = (RadioButton) view.findViewById(R.id.radioFemale);
        radioFemale.setText(trad.getTexto("Mujer"));
        radioFemale.setTypeface(miso_font);
        RadioButton radioEstudiante = (RadioButton) view.findViewById(R.id.radioEstudiante);
        radioEstudiante.setText(trad.getTexto("Estudiante"));
        radioEstudiante.setTypeface(miso_font);
        RadioButton radioProfesor = (RadioButton) view.findViewById(R.id.radioProfesor);
        radioProfesor.setText(trad.getTexto("Profesor o profesora"));
        radioProfesor.setTypeface(miso_font);
        RadioButton radioTurista = (RadioButton) view.findViewById(R.id.radioTurista);
        radioTurista.setText(trad.getTexto("Turista"));
        radioTurista.setTypeface(miso_font);
        RadioButton radioOtro = (RadioButton) view.findViewById(R.id.radioOtro);
        radioOtro.setText(trad.getTexto("Otro"));
        radioOtro.setTypeface(miso_font);
        RadioButton radioInternet = (RadioButton) view.findViewById(R.id.radioInternet);
        radioInternet.setText(trad.getTexto("Internet"));
        radioInternet.setTypeface(miso_font);
        RadioButton radioMediosComunicacion = (RadioButton) view.findViewById(R.id.radioMediosComunicacion);
        radioMediosComunicacion.setText(trad.getTexto("Medios de comunicacion"));
        radioMediosComunicacion.setTypeface(miso_font);
        RadioButton radioVisitaGrupo = (RadioButton) view.findViewById(R.id.radioVisitaGrupo);
        radioVisitaGrupo.setText(trad.getTexto("Visita en grupo"));
        radioVisitaGrupo.setTypeface(miso_font);
        RadioButton radioOtroConoci = (RadioButton) view.findViewById(R.id.radioOtroConoci);
        radioOtroConoci.setText(trad.getTexto("Otro"));
        radioOtroConoci.setTypeface(miso_font);

        soy_edit = (EditText) view.findViewById(R.id.soy_edit);
        soy_edit.setVisibility(View.INVISIBLE);

        soy_edit.setOnEditorActionListener(this);
        soy_edit.setRawInputType(InputType.TYPE_CLASS_TEXT);
        soy_edit.setImeOptions(EditorInfo.IME_ACTION_GO);

        conoci_edit = (EditText) view.findViewById(R.id.conoci_edit);
        conoci_edit.setVisibility(View.INVISIBLE);

        conoci_edit.setOnEditorActionListener(this);
        conoci_edit.setRawInputType(InputType.TYPE_CLASS_TEXT);
        conoci_edit.setImeOptions(EditorInfo.IME_ACTION_GO);
    }
}
