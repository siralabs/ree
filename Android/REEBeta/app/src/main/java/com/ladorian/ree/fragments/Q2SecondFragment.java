package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.models.Cuestionario;

import java.util.Arrays;
import java.util.List;

public class Q2SecondFragment extends Fragment {
    private static Typeface miso_font;
    private Traducciones trad;
    private View rootView;
    final List<String> answers1 = Arrays.asList("Muy Malo", "Malo", "Bueno", "Muy Bueno");
    final List<String> answers2 = Arrays.asList("Muy Malo", "Malo", "Bueno", "Muy Bueno", "No participe");

    private ImageButton refreshOne;
    private ImageButton dislikeOne;
    private ImageButton likeOne;
    private ImageButton refreshTwo;
    private ImageButton dislikeTwo;
    private ImageButton likeTwo;
    private ImageButton refreshThree;
    private ImageButton dislikeThree;
    private ImageButton likeThree;
    private ImageButton refreshFour;
    private ImageButton dislikeFour;
    private ImageButton likeFour;
    private int q2;
    private int q3;
    private int q4;
    private int q5;
    private int q6;
    private int q7;
    private String q8;
    public int q81;
    public int q82;
    public int q83;
    public int q84;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final CuestionarioActivity activity = (CuestionarioActivity) getActivity();
        rootView = inflater.inflate(R.layout.q2_second_fragment, container, false);
        traducirFragment(activity, rootView);

        final Q2SecondFragment weakSelf = this;
        configurarEventos(activity, rootView);
        colocarSituacionAnterior(activity, rootView);
        configurarNavegacion(activity, rootView);
        return rootView;
    }

    private void configurarEventos(final CuestionarioActivity activity, final View view) {

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek2_1, R.id.answer2, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek2_2, R.id.answer2, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek2_3, R.id.answer2, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek2_4, R.id.answer2, answers1, 3);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek3_1, R.id.answer3, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek3_2, R.id.answer3, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek3_3, R.id.answer3, answers1, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek3_4, R.id.answer3, answers1, 3);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek4_1, R.id.answer4, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek4_2, R.id.answer4, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek4_3, R.id.answer4, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek4_4, R.id.answer4, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek4_5, R.id.answer4, answers2, 3);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek5_1, R.id.answer5, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek5_2, R.id.answer5, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek5_3, R.id.answer5, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek5_4, R.id.answer5, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek5_5, R.id.answer5, answers2, 3);


        enlazarSeekbarButtonAnswer(activity, view, R.id.seek6_1, R.id.answer6, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek6_2, R.id.answer6, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek6_3, R.id.answer6, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek6_4, R.id.answer6, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek6_5, R.id.answer6, answers2, 3);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek7_1, R.id.answer7, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek7_2, R.id.answer7, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek7_3, R.id.answer7, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek7_4, R.id.answer7, answers2, 3);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek7_5, R.id.answer7, answers2, 3);
        refreshOne = (ImageButton) rootView.findViewById(R.id.refresh_one);
        refreshTwo = (ImageButton) rootView.findViewById(R.id.refresh_two);
        refreshThree = (ImageButton) rootView.findViewById(R.id.refresh_three);
        refreshFour = (ImageButton) rootView.findViewById(R.id.refresh_four);
        likeOne = (ImageButton) rootView.findViewById(R.id.like_one);
        likeTwo = (ImageButton) rootView.findViewById(R.id.like_two);
        likeThree = (ImageButton) rootView.findViewById(R.id.like_three);
        likeFour = (ImageButton) rootView.findViewById(R.id.like_four);
        dislikeOne = (ImageButton) rootView.findViewById(R.id.dislike_one);
        dislikeTwo = (ImageButton) rootView.findViewById(R.id.dislike_two);
        dislikeThree = (ImageButton) rootView.findViewById(R.id.dislike_three);
        dislikeFour = (ImageButton) rootView.findViewById(R.id.dislike_four);
        likeOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeOne.setVisibility(View.INVISIBLE);
                refreshOne.setVisibility(View.VISIBLE);
                q81 = 1;

            }
        });

        dislikeOne.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeOne.setVisibility(View.INVISIBLE);
                refreshOne.setVisibility(View.VISIBLE);
                q81 = -1;

            }
        });

        refreshOne.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q81 = 0;

            }
        });

        likeTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeTwo.setVisibility(View.INVISIBLE);
                refreshTwo.setVisibility(View.VISIBLE);
                q82 = 1;

            }
        });

        dislikeTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeTwo.setVisibility(View.INVISIBLE);
                refreshTwo.setVisibility(View.VISIBLE);
                q82 = -1;

            }
        });

        refreshTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q82 = 0;

            }
        });

        likeThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeThree.setVisibility(View.INVISIBLE);
                refreshThree.setVisibility(View.VISIBLE);
                q83 = 1;

            }
        });

        dislikeThree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeThree.setVisibility(View.INVISIBLE);
                refreshThree.setVisibility(View.VISIBLE);
                q83 = -1;

            }
        });

        refreshThree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q83 = 0;

            }
        });

        likeFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeFour.setVisibility(View.INVISIBLE);
                refreshFour.setVisibility(View.VISIBLE);
                q84 = 1;

            }
        });

        dislikeFour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeFour.setVisibility(View.INVISIBLE);
                refreshFour.setVisibility(View.VISIBLE);
                q84 = -1;

            }
        });

        refreshFour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q84 = 0;

            }
        });
        checkPartials();

    }


    private void enlazarSeekbarButtonAnswer(final CuestionarioActivity activity, final View view, final int idButton, int idAnswer, final List<String> answersArray, int defaultValue) {
        final TextView answerText = (TextView) view.findViewById(idAnswer);
        answerText.setText(trad.getTexto(answersArray.get(defaultValue)));

        final Button sk = (Button) view.findViewById(idButton);

        sk.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnButton(view, idButton, answerText, answersArray);
            }


        });
    }

    private void clickOnButton(final View view, int idButtonPressed, TextView answerText, final List<String> answersArray) {
        answerText.setText("");
        if (idButtonPressed == R.id.seek2_1 || idButtonPressed == R.id.seek2_2 || idButtonPressed == R.id.seek2_3 || idButtonPressed == R.id.seek2_4) {
            q2 = 0;
            q2 = q2 + setQuestionDisabled(view, R.id.seek2_1, idButtonPressed, answerText, answersArray, 0);
            q2 = q2 + setQuestionDisabled(view, R.id.seek2_2, idButtonPressed, answerText, answersArray, 1);
            q2 = q2 + setQuestionDisabled(view, R.id.seek2_3, idButtonPressed, answerText, answersArray, 2);
            q2 = q2 + setQuestionDisabled(view, R.id.seek2_4, idButtonPressed, answerText, answersArray, 3);
        }
        if (idButtonPressed == R.id.seek3_1 || idButtonPressed == R.id.seek3_2 || idButtonPressed == R.id.seek3_3 || idButtonPressed == R.id.seek3_4) {
            q3 = 0;
            q3 = q3 + setQuestionDisabled(view, R.id.seek3_1, idButtonPressed, answerText, answersArray, 0);
            q3 = q3 + setQuestionDisabled(view, R.id.seek3_2, idButtonPressed, answerText, answersArray, 1);
            q3 = q3 + setQuestionDisabled(view, R.id.seek3_3, idButtonPressed, answerText, answersArray, 2);
            q3 = q3 + setQuestionDisabled(view, R.id.seek3_4, idButtonPressed, answerText, answersArray, 3);
        }
        if (idButtonPressed == R.id.seek4_1 || idButtonPressed == R.id.seek4_2 || idButtonPressed == R.id.seek4_3 || idButtonPressed == R.id.seek4_4 || idButtonPressed == R.id.seek4_5) {
            q4 = 0;
            q4 = q4 + setQuestionDisabled(view, R.id.seek4_1, idButtonPressed, answerText, answersArray, 0);
            q4 = q4 + setQuestionDisabled(view, R.id.seek4_2, idButtonPressed, answerText, answersArray, 1);
            q4 = q4 + setQuestionDisabled(view, R.id.seek4_3, idButtonPressed, answerText, answersArray, 2);
            q4 = q4 + setQuestionDisabled(view, R.id.seek4_4, idButtonPressed, answerText, answersArray, 3);
            q4 = q4 + setQuestionDisabled(view, R.id.seek4_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek5_1 || idButtonPressed == R.id.seek5_2 || idButtonPressed == R.id.seek5_3 || idButtonPressed == R.id.seek5_4 || idButtonPressed == R.id.seek5_5) {
            q5 = 0;
            q5 = q5 + setQuestionDisabled(view, R.id.seek5_1, idButtonPressed, answerText, answersArray, 0);
            q5 = q5 + setQuestionDisabled(view, R.id.seek5_2, idButtonPressed, answerText, answersArray, 1);
            q5 = q5 + setQuestionDisabled(view, R.id.seek5_3, idButtonPressed, answerText, answersArray, 2);
            q5 = q5 + setQuestionDisabled(view, R.id.seek5_4, idButtonPressed, answerText, answersArray, 3);
            q5 = q5 + setQuestionDisabled(view, R.id.seek5_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek6_1 || idButtonPressed == R.id.seek6_2 || idButtonPressed == R.id.seek6_3 || idButtonPressed == R.id.seek6_4 || idButtonPressed == R.id.seek6_5) {
            q6 = 0;
            q6 = q6 + setQuestionDisabled(view, R.id.seek6_1, idButtonPressed, answerText, answersArray, 0);
            q6 = q6 + setQuestionDisabled(view, R.id.seek6_2, idButtonPressed, answerText, answersArray, 1);
            q6 = q6 + setQuestionDisabled(view, R.id.seek6_3, idButtonPressed, answerText, answersArray, 2);
            q6 = q6 + setQuestionDisabled(view, R.id.seek6_4, idButtonPressed, answerText, answersArray, 3);
            q6 = q6 + setQuestionDisabled(view, R.id.seek6_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek7_1 || idButtonPressed == R.id.seek7_2 || idButtonPressed == R.id.seek7_3 || idButtonPressed == R.id.seek7_4 || idButtonPressed == R.id.seek7_5) {
            q7 = 0;
            q7 = q7 + setQuestionDisabled(view, R.id.seek7_1, idButtonPressed, answerText, answersArray, 0);
            q7 = q7 + setQuestionDisabled(view, R.id.seek7_2, idButtonPressed, answerText, answersArray, 1);
            q7 = q7 + setQuestionDisabled(view, R.id.seek7_3, idButtonPressed, answerText, answersArray, 2);
            q7 = q7 + setQuestionDisabled(view, R.id.seek7_4, idButtonPressed, answerText, answersArray, 3);
            q7 = q7 + setQuestionDisabled(view, R.id.seek7_5, idButtonPressed, answerText, answersArray, 4);
        }
    }

    private int setQuestionDisabled(final View view, int idToDisable, int idButtonPressed, TextView answerText, final List<String> answersArray, int valueToEnable) {
        int value = 0;
        if (idToDisable != idButtonPressed) {
            if (idToDisable != 0) {
                Button skb1 = (Button) view.findViewById(idToDisable);
                if (skb1 != null) {
                    skb1.setBackgroundResource(R.drawable.multistatebutton);
                }
            }
        } else {
            if (idButtonPressed != 0) {
                Button skb2 = (Button) view.findViewById(idButtonPressed);
                skb2.setBackgroundResource(R.drawable.multistatebutton2);
            }
            value = valueToEnable;
            answerText.setText(trad.getTexto(answersArray.get(value)));
        }
        return value;
    }

    private void configurarNavegacion(final CuestionarioActivity activity, final View view) {

        ImageButton next_btn = (ImageButton) view.findViewById(R.id.next_btn);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAnswers(view)) {
                    activity.getmCuestionario().setQ2(q2);
                    activity.getmCuestionario().setQ3(q3);
                    activity.getmCuestionario().setQ4(q4);
                    activity.getmCuestionario().setQ5(q5);
                    activity.getmCuestionario().setQ6(q6);
                    activity.getmCuestionario().setQ7(q7);
                    activity.getmCuestionario().setQ8(q8);
                    activity.getmCuestionario().setQ81(q81);
                    activity.getmCuestionario().setQ82(q82);
                    activity.getmCuestionario().setQ83(q83);
                    activity.getmCuestionario().setQ84(q84);
                    activity.changeQuestion(2);
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });
        ImageButton prev_btn = (ImageButton) view.findViewById(R.id.prev_btn);
        prev_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAnswers(view)) {
                    activity.getmCuestionario().setQ2(q2);
                    activity.getmCuestionario().setQ3(q3);
                    activity.getmCuestionario().setQ4(q4);
                    activity.getmCuestionario().setQ5(q5);
                    activity.getmCuestionario().setQ6(q6);
                    activity.getmCuestionario().setQ7(q7);
                    activity.getmCuestionario().setQ8(q8);
                    activity.getmCuestionario().setQ81(q81);
                    activity.getmCuestionario().setQ82(q82);
                    activity.getmCuestionario().setQ83(q83);
                    activity.getmCuestionario().setQ84(q84);
                    activity.changeQuestion(0);
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void colocarSituacionAnterior(CuestionarioActivity activity, View view) {
        Cuestionario cuestionario = activity.getmCuestionario();
        q2 = cuestionario.getQ2();
        q3 = cuestionario.getQ3();
        q4 = cuestionario.getQ4();
        q5 = cuestionario.getQ5();
        q6 = cuestionario.getQ6();
        q7 = cuestionario.getQ7();
        if (cuestionario.getQ2() == -1) {
            cuestionario.setQ2(3);
        }
        if (cuestionario.getQ3() == -1) {
            cuestionario.setQ3(3);
        }
        if (cuestionario.getQ4() == -1) {
            cuestionario.setQ4(4);
        }
        if (cuestionario.getQ5() == -1) {
            cuestionario.setQ5(4);
        }
        if (cuestionario.getQ6() == -1) {
            cuestionario.setQ6(4);
        }
        if (cuestionario.getQ7() == -1) {
            cuestionario.setQ7(4);
        }

        int idSeek2 = getResources().getIdentifier("seek2_" + (cuestionario.getQ2() + 1), "id", activity.getPackageName());
        int idSeek3 = getResources().getIdentifier("seek3_" + (cuestionario.getQ3() + 1), "id", activity.getPackageName());
        int idSeek4 = getResources().getIdentifier("seek4_" + (cuestionario.getQ4() + 1), "id", activity.getPackageName());
        int idSeek5 = getResources().getIdentifier("seek5_" + (cuestionario.getQ5() + 1), "id", activity.getPackageName());
        int idSeek6 = getResources().getIdentifier("seek6_" + (cuestionario.getQ6() + 1), "id", activity.getPackageName());
        int idSeek7 = getResources().getIdentifier("seek7_" + (cuestionario.getQ7() + 1), "id", activity.getPackageName());
        final TextView answerText2 = (TextView) view.findViewById(R.id.answer2);
        final TextView answerText3 = (TextView) view.findViewById(R.id.answer3);
        final TextView answerText4 = (TextView) view.findViewById(R.id.answer4);
        final TextView answerText5 = (TextView) view.findViewById(R.id.answer5);
        final TextView answerText6 = (TextView) view.findViewById(R.id.answer6);
        final TextView answerText7 = (TextView) view.findViewById(R.id.answer7);
        setQuestionDisabled(view, idSeek2, idSeek2, answerText2, answers1, cuestionario.getQ2());
        setQuestionDisabled(view, idSeek3, idSeek3, answerText3, answers1, cuestionario.getQ3());
        setQuestionDisabled(view, idSeek4, idSeek4, answerText4, answers2, cuestionario.getQ4());
        setQuestionDisabled(view, idSeek5, idSeek5, answerText5, answers2, cuestionario.getQ5());
        setQuestionDisabled(view, idSeek6, idSeek6, answerText6, answers2, cuestionario.getQ6());
        setQuestionDisabled(view, idSeek7, idSeek7, answerText7, answers2, cuestionario.getQ7());
        //   setValueSelectedSeekbar(view, R.id.seekBar2, R.id.answer2, answers1, cuestionario.getQ2(), 3);
        //    setValueSelectedSeekbar(view, R.id.seekBar3, R.id.answer3, answers1, cuestionario.getQ3(), 3);
//        setValueSelectedSeekbar(view, R.id.seekBar4, R.id.answer4, answers2, cuestionario.getQ4(), 4);
//        setValueSelectedSeekbar(view, R.id.seekBar5, R.id.answer5, answers2, cuestionario.getQ5(), 4);
//        setValueSelectedSeekbar(view, R.id.seekBar6, R.id.answer6, answers2, cuestionario.getQ6(), 4);
//        setValueSelectedSeekbar(view, R.id.seekBar7, R.id.answer7, answers2, cuestionario.getQ7(), 4);

        setValueSelectedImageButton(view, R.id.dislike_one, R.id.like_one, R.id.refresh_one, cuestionario.getQ81());
        setValueSelectedImageButton(view, R.id.dislike_two, R.id.like_two, R.id.refresh_two, cuestionario.getQ82());
        setValueSelectedImageButton(view, R.id.dislike_three, R.id.like_three, R.id.refresh_three, cuestionario.getQ83());
        setValueSelectedImageButton(view, R.id.dislike_four, R.id.like_four, R.id.refresh_four, cuestionario.getQ84());
        q81 = cuestionario.getQ81();
        q82 = cuestionario.getQ82();
        q83 = cuestionario.getQ83();
        q84 = cuestionario.getQ84();

    }

    private void setValueSelectedImageButton(View view, int id_dislike, int id_like, int id_refresh, int answer) {
        ImageButton refresh = (ImageButton) rootView.findViewById(id_refresh);
        ImageButton like = (ImageButton) rootView.findViewById(id_like);
        ImageButton dislike = (ImageButton) rootView.findViewById(id_dislike);
        switch (answer) {
            case -1:
                refresh.setVisibility(View.VISIBLE);
                like.setVisibility(View.INVISIBLE);
                dislike.setVisibility(View.VISIBLE);
                break;
            case 0:
                refresh.setVisibility(View.INVISIBLE);
                like.setVisibility(View.VISIBLE);
                dislike.setVisibility(View.VISIBLE);
                break;
            case 1:
                refresh.setVisibility(View.VISIBLE);
                like.setVisibility(View.VISIBLE);
                dislike.setVisibility(View.INVISIBLE);
                break;
        }

    }

    private Boolean checkAnswers(View view) {
        Boolean checked = true;
        int sumaq8 = q81 + q82 + q83 + q84;
        switch (sumaq8) {
            case -4:
                q8 = "NADA";
                break;
            case 4:
                q8 = "TODO";
                break;
            default:
                q8 = "";
                break;
        }
        if (q81 == 0 && q82 == 0 && q83 == 0 && q84 == 0) {
            checked = false;
        }
        //    q2 = getValueSelectedSeekbar(view, R.id.seekBar2, -1);
//        q3 = getValueSelectedSeekbar(view, R.id.seekBar3, -1);
//        q4 = getValueSelectedSeekbar(view, R.id.seekBar4, -1);
//        q5 = getValueSelectedSeekbar(view, R.id.seekBar5, -1);
//        q6 = getValueSelectedSeekbar(view, R.id.seekBar6, -1);
//        q7 = getValueSelectedSeekbar(view, R.id.seekBar7, -1);


        return checked;
    }

    private int getValueSelectedSeekbar(View view, int idSeekbar, int indexNotAswered) {
        final SeekBar sk = (SeekBar) view.findViewById(idSeekbar);

        int value = sk.getProgress();
        if (value == indexNotAswered) {
            value = -1;
        }
        return value;
    }

    private void setValueSelectedSeekbarButton(View view, int idSeekbar, int idAnswer, List<String> answers, int value, int indexDefault) {
        final SeekBar sk = (SeekBar) view.findViewById(idSeekbar);
        if (value == -1) {
            value = indexDefault;
        }
        sk.setProgress(value);
        final TextView answerText = (TextView) view.findViewById(idAnswer);
        answerText.setText(trad.getTexto(answers.get(value)));
    }

    protected void traducirPregunta(Activity activity, View view, int id, String pregunta, List<String> respuestas) {
        TextView preguntaText = (TextView) view.findViewById(id);
        preguntaText.setTypeface(miso_font);
        preguntaText.setText(trad.getTexto(pregunta));

    }

    protected void traducirFragment(Activity activity, View view) {
        trad = new Traducciones(activity);
        miso_font = Typeface.createFromAsset(activity.getAssets(), "fonts/miso.otf");


        traducirPregunta(activity, view, R.id.question2, "Opinion general de la exposicion", answers1);
        traducirPregunta(activity, view, R.id.question3, "Claridad de la informacion", answers1);
        traducirPregunta(activity, view, R.id.question4, "Explicacion de los monitores", answers1);
        traducirPregunta(activity, view, R.id.question5, "Talleres", answers1);
        traducirPregunta(activity, view, R.id.question6, "Guias didacticas", answers1);
        traducirPregunta(activity, view, R.id.question7, "Videos", answers1);
        traducirPregunta(activity, view, R.id.experiencia_two, "Maqueta sistema electrico", answers1);
        traducirPregunta(activity, view, R.id.experiencia_three, "Piezas y material electrico", answers1);
        traducirPregunta(activity, view, R.id.experiencia_one, "Mesa de experimentos", answers1);
        traducirPregunta(activity, view, R.id.experiencia_four, "Interactivos", answers1);


        ImageButton nextBtn = (ImageButton) view.findViewById(R.id.next_btn);
        ImageButton prevBtn = (ImageButton) view.findViewById(R.id.prev_btn);
        switch (trad.getIdioma()) {
            case "castellano":
                prevBtn.setImageResource(R.drawable.prev_arrow);
                nextBtn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                prevBtn.setImageResource(R.drawable.prev_arrow_cat);
                nextBtn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                prevBtn.setImageResource(R.drawable.prev_arrow_en);
                nextBtn.setImageResource(R.drawable.next_arrow_en);
                break;
        }

    }

    public void checkPartials() {
        switch (q81) {
            case -1:
                refreshOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                likeOne.setVisibility(View.INVISIBLE);

                break;
            case 0:
                refreshOne.setVisibility(View.INVISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                likeOne.setVisibility(View.VISIBLE);

                break;
            case 1:
                refreshOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.INVISIBLE);
                likeOne.setVisibility(View.VISIBLE);

                break;
        }
        switch (q82) {
            case -1:
                refreshTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                likeTwo.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshTwo.setVisibility(View.INVISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                likeTwo.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.INVISIBLE);
                likeTwo.setVisibility(View.VISIBLE);
                break;
        }
        switch (q83) {
            case -1:
                refreshThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                likeThree.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshThree.setVisibility(View.INVISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                likeThree.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.INVISIBLE);
                likeThree.setVisibility(View.VISIBLE);
                break;
        }
        switch (q84) {
            case -1:
                refreshFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                likeFour.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshFour.setVisibility(View.INVISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                likeFour.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.INVISIBLE);
                likeFour.setVisibility(View.VISIBLE);
                break;
        }
    }

}
