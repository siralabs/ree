package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.models.Cuestionario;

import java.util.Arrays;
import java.util.List;

public class Q2ThirdFragment extends Fragment {
    private static Typeface miso_font;
    private EditText soy_edit;
    private EditText conoci_edit;
    private Traducciones trad;
    private int q9;
    private int q10;
    private int q11;
    private int q12;
    private int q13;
    private String q14;

    private View rootView;
    final List<String> answers2 = Arrays.asList("No se", "Mala", "Regular", "Buena", "Excelente");
    final List<String> answers3 = Arrays.asList("No se", "Innecesarias", "Poco Necesarias", "Necesarias", "Imprescindibles");
    final List<String> answers4 = Arrays.asList("No se", "Malo", "Regular", "Bueno", "Excelente");

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final CuestionarioActivity activity = (CuestionarioActivity) getActivity();
        rootView = inflater.inflate(R.layout.q2_third_fragment, container, false);
        traducirFragment(activity, rootView);

        final Q2ThirdFragment weakSelf = this;
        configurarEventos(activity, rootView);
        colocarSituacionAnterior(activity, rootView);
        configurarNavegacion(activity, rootView);
        return rootView;
    }

    private void configurarEventos(final CuestionarioActivity activity, final View view) {
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek9_1, R.id.answer9, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek9_2, R.id.answer9, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek9_3, R.id.answer9, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek9_4, R.id.answer9, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek9_5, R.id.answer9, answers2, 0);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek10_1, R.id.answer10, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek10_2, R.id.answer10, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek10_3, R.id.answer10, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek10_4, R.id.answer10, answers2, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek10_5, R.id.answer10, answers2, 0);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek11_1, R.id.answer11, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek11_2, R.id.answer11, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek11_3, R.id.answer11, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek11_4, R.id.answer11, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek11_5, R.id.answer11, answers3, 0);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek12_1, R.id.answer12, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek12_2, R.id.answer12, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek12_3, R.id.answer12, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek12_4, R.id.answer12, answers3, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek12_5, R.id.answer12, answers3, 0);

        enlazarSeekbarButtonAnswer(activity, view, R.id.seek13_1, R.id.answer13, answers4, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek13_2, R.id.answer13, answers4, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek13_3, R.id.answer13, answers4, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek13_4, R.id.answer13, answers4, 0);
        enlazarSeekbarButtonAnswer(activity, view, R.id.seek13_5, R.id.answer13, answers4, 0);

    }


    private void enlazarSeekbarButtonAnswer(final CuestionarioActivity activity, final View view, final int idButton, int idAnswer, final List<String> answersArray, int defaultValue) {
        final TextView answerText = (TextView) view.findViewById(idAnswer);
        answerText.setText(trad.getTexto(answersArray.get(defaultValue)));

        final Button sk = (Button) view.findViewById(idButton);

        sk.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnButton(view, idButton, answerText, answersArray);
            }


        });
    }

    private void clickOnButton(final View view, int idButtonPressed, TextView answerText, final List<String> answersArray) {
        answerText.setText("");
        if (idButtonPressed == R.id.seek9_1 || idButtonPressed == R.id.seek9_2 || idButtonPressed == R.id.seek9_3 || idButtonPressed == R.id.seek9_4 || idButtonPressed == R.id.seek9_5) {
            q9 = 0;
            q9 = q9 + setQuestionDisabled(view, R.id.seek9_1, idButtonPressed, answerText, answersArray, 0);
            q9 = q9 + setQuestionDisabled(view, R.id.seek9_2, idButtonPressed, answerText, answersArray, 1);
            q9 = q9 + setQuestionDisabled(view, R.id.seek9_3, idButtonPressed, answerText, answersArray, 2);
            q9 = q9 + setQuestionDisabled(view, R.id.seek9_4, idButtonPressed, answerText, answersArray, 3);
            q9 = q9 + setQuestionDisabled(view, R.id.seek9_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek10_1 || idButtonPressed == R.id.seek10_2 || idButtonPressed == R.id.seek10_3 || idButtonPressed == R.id.seek10_4|| idButtonPressed == R.id.seek10_5) {
            q10 = 0;
            q10 = q10 + setQuestionDisabled(view, R.id.seek10_1, idButtonPressed, answerText, answersArray, 0);
            q10 = q10 + setQuestionDisabled(view, R.id.seek10_2, idButtonPressed, answerText, answersArray, 1);
            q10 = q10 + setQuestionDisabled(view, R.id.seek10_3, idButtonPressed, answerText, answersArray, 2);
            q10 = q10 + setQuestionDisabled(view, R.id.seek10_4, idButtonPressed, answerText, answersArray, 3);
            q10 = q10 + setQuestionDisabled(view, R.id.seek10_5, idButtonPressed, answerText, answersArray, 4);

        }
        if (idButtonPressed == R.id.seek11_1 || idButtonPressed == R.id.seek11_2 || idButtonPressed == R.id.seek11_3 || idButtonPressed == R.id.seek11_4 || idButtonPressed == R.id.seek11_5) {
            q11 = 0;
            q11 = q11 + setQuestionDisabled(view, R.id.seek11_1, idButtonPressed, answerText, answersArray, 0);
            q11 = q11 + setQuestionDisabled(view, R.id.seek11_2, idButtonPressed, answerText, answersArray, 1);
            q11 = q11 + setQuestionDisabled(view, R.id.seek11_3, idButtonPressed, answerText, answersArray, 2);
            q11 = q11 + setQuestionDisabled(view, R.id.seek11_4, idButtonPressed, answerText, answersArray, 3);
            q11 = q11 + setQuestionDisabled(view, R.id.seek11_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek12_1 || idButtonPressed == R.id.seek12_2 || idButtonPressed == R.id.seek12_3 || idButtonPressed == R.id.seek12_4 || idButtonPressed == R.id.seek12_5) {
            q12 = 0;
            q12 = q12 + setQuestionDisabled(view, R.id.seek12_1, idButtonPressed, answerText, answersArray, 0);
            q12 = q12 + setQuestionDisabled(view, R.id.seek12_2, idButtonPressed, answerText, answersArray, 1);
            q12 = q12 + setQuestionDisabled(view, R.id.seek12_3, idButtonPressed, answerText, answersArray, 2);
            q12 = q12 + setQuestionDisabled(view, R.id.seek12_4, idButtonPressed, answerText, answersArray, 3);
            q12 = q12 + setQuestionDisabled(view, R.id.seek12_5, idButtonPressed, answerText, answersArray, 4);
        }
        if (idButtonPressed == R.id.seek13_1 || idButtonPressed == R.id.seek13_2 || idButtonPressed == R.id.seek13_3 || idButtonPressed == R.id.seek13_4 || idButtonPressed == R.id.seek13_5) {
            q13 = 0;
            q13 = q13 + setQuestionDisabled(view, R.id.seek13_1, idButtonPressed, answerText, answersArray, 0);
            q13 = q13 + setQuestionDisabled(view, R.id.seek13_2, idButtonPressed, answerText, answersArray, 1);
            q13 = q13 + setQuestionDisabled(view, R.id.seek13_3, idButtonPressed, answerText, answersArray, 2);
            q13 = q13 + setQuestionDisabled(view, R.id.seek13_4, idButtonPressed, answerText, answersArray, 3);
            q13 = q13 + setQuestionDisabled(view, R.id.seek13_5, idButtonPressed, answerText, answersArray, 4);
        }

    }

    private int setQuestionDisabled(final View view, int idToDisable, int idButtonPressed, TextView answerText, final List<String> answersArray, int valueToEnable) {
        int value = 0;
        if (idToDisable != idButtonPressed) {
            if (idToDisable != 0) {
                Button skb1 = (Button) view.findViewById(idToDisable);
                if (skb1 != null) {
                    skb1.setBackgroundResource(R.drawable.multistatebutton);
                }
            }
        } else {
            if (idButtonPressed != 0) {
                Button skb2 = (Button) view.findViewById(idButtonPressed);
                skb2.setBackgroundResource(R.drawable.multistatebutton2);
            }
            value = valueToEnable;
            answerText.setText(trad.getTexto(answersArray.get(value)));
        }
        return value;
    }

    private void configurarNavegacion(final CuestionarioActivity activity, final View view) {

        ImageButton send_btn = (ImageButton) view.findViewById(R.id.send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAnswers(view)) {
                    activity.getmCuestionario().setQ9(q9);
                    activity.getmCuestionario().setQ10(q10);
                    activity.getmCuestionario().setQ11(q11);
                    activity.getmCuestionario().setQ12(q12);
                    activity.getmCuestionario().setQ13(q13);
                    activity.getmCuestionario().setQ14(q14);
                    activity.sendCuestionario();
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });
        ImageButton prev_btn = (ImageButton) view.findViewById(R.id.prev_btn);
        prev_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAnswers(view)) {
                    activity.getmCuestionario().setQ9(q9);
                    activity.getmCuestionario().setQ10(q10);
                    activity.getmCuestionario().setQ11(q11);
                    activity.getmCuestionario().setQ12(q12);
                    activity.getmCuestionario().setQ13(q13);
                    activity.getmCuestionario().setQ14(q14);
                    activity.changeQuestion(1);
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void colocarSituacionAnterior(CuestionarioActivity activity, View view) {
        Cuestionario cuestionario = activity.getmCuestionario();
        q9 = cuestionario.getQ9();
        q10 = cuestionario.getQ10();
        q11 = cuestionario.getQ11();
        q12 = cuestionario.getQ12();
        q13 = cuestionario.getQ13();

        if (cuestionario.getQ9() == -1) {
            cuestionario.setQ9(0);
        }
        if (cuestionario.getQ10() == -1) {
            cuestionario.setQ10(0);
        }
        if (cuestionario.getQ11() == -1) {
            cuestionario.setQ11(0);
        }
        if (cuestionario.getQ12() == -1) {
            cuestionario.setQ12(0);
        }
        if (cuestionario.getQ13() == -1) {
            cuestionario.setQ13(0);
        }

        int idSeek9 = getResources().getIdentifier("seek9_" + (cuestionario.getQ9() + 1), "id", activity.getPackageName());
        int idSeek10 = getResources().getIdentifier("seek10_" + (cuestionario.getQ10() + 1), "id", activity.getPackageName());
        int idSeek11 = getResources().getIdentifier("seek11_" + (cuestionario.getQ11() + 1), "id", activity.getPackageName());
        int idSeek12 = getResources().getIdentifier("seek12_" + (cuestionario.getQ12() + 1), "id", activity.getPackageName());
        int idSeek13 = getResources().getIdentifier("seek13_" + (cuestionario.getQ13() + 1), "id", activity.getPackageName());

        final TextView answerText9 = (TextView) view.findViewById(R.id.answer9);
        final TextView answerText10 = (TextView) view.findViewById(R.id.answer10);
        final TextView answerText11 = (TextView) view.findViewById(R.id.answer11);
        final TextView answerText12 = (TextView) view.findViewById(R.id.answer12);
        final TextView answerText13 = (TextView) view.findViewById(R.id.answer13);


        setQuestionDisabled(view, idSeek9, idSeek9, answerText9, answers2, cuestionario.getQ9());
        setQuestionDisabled(view, idSeek10, idSeek10, answerText10, answers2, cuestionario.getQ10());
        setQuestionDisabled(view, idSeek11, idSeek11, answerText11, answers3, cuestionario.getQ11());
        setQuestionDisabled(view, idSeek12, idSeek12, answerText12, answers3, cuestionario.getQ12());
        setQuestionDisabled(view, idSeek13, idSeek13, answerText13, answers4, cuestionario.getQ13());


        EditText answer14Edit = (EditText) view.findViewById(R.id.answer14);
        answer14Edit.setText(cuestionario.getQ14());

    }

    private Boolean checkAnswers(View view) {
        Boolean checked = true;

//        q9 = getValueSelectedSeekbar(view, R.id.seekBar9, -1);
//        q10 = getValueSelectedSeekbar(view, R.id.seekBar10, -1);
//        q11 = getValueSelectedSeekbar(view, R.id.seekBar11, -1);
//        q12 = getValueSelectedSeekbar(view, R.id.seekBar12, -1);
//        q13 = getValueSelectedSeekbar(view, R.id.seekBar13, -1);
        EditText answer14Edit = (EditText) view.findViewById(R.id.answer14);
        q14 = answer14Edit.getText().toString();

        return checked;
    }

    private int getValueSelectedSeekbar(View view, int idSeekbar, int indexNotAswered) {
        final SeekBar sk = (SeekBar) view.findViewById(idSeekbar);

        int value = sk.getProgress();
        if (value == indexNotAswered) {
            value = -1;
        }
        return value;
    }

    private void setValueSelectedSeekbar(View view, int idSeekbar, int idAnswer, List<String> answers, int value, int indexDefault) {
        final SeekBar sk = (SeekBar) view.findViewById(idSeekbar);
        if (value == -1) {
            value = indexDefault;
        }

        sk.setProgress(value);
        final TextView answerText = (TextView) view.findViewById(idAnswer);
        answerText.setText(trad.getTexto(answers.get(value)));


    }

    protected void traducirPregunta(Activity activity, View view, int id, String pregunta, List<String> respuestas) {
        TextView preguntaText = (TextView) view.findViewById(id);
        preguntaText.setTypeface(miso_font);
        preguntaText.setText(Html.fromHtml(trad.getTexto(pregunta)));

    }

    protected void traducirFragment(Activity activity, View view) {
        trad = new Traducciones(activity);
        miso_font = Typeface.createFromAsset(activity.getAssets(), "fonts/miso.otf");


        traducirPregunta(activity, view, R.id.question9, "Antes de ver la exposicion", answers2);
        traducirPregunta(activity, view, R.id.question10, "Despues de ver la exposicion", answers2);
        traducirPregunta(activity, view, R.id.question11, "Antes de ver la exposicion2", answers3);
        traducirPregunta(activity, view, R.id.question12, "Despues de ver la exposicion2", answers3);
        traducirPregunta(activity, view, R.id.question13, "Valore el compromiso de Red Electrica de Espana con la sostenibilidad y el medio ambiente", answers4);
        traducirPregunta(activity, view, R.id.question14, "comentariosugerencia", null);


        ImageButton sendBtn = (ImageButton) view.findViewById(R.id.send_btn);
        ImageButton prevBtn = (ImageButton) view.findViewById(R.id.prev_btn);
        switch (trad.getIdioma()) {
            case "castellano":
                prevBtn.setImageResource(R.drawable.prev_arrow);
                sendBtn.setImageResource(R.drawable.send);
                break;
            case "catala":
                prevBtn.setImageResource(R.drawable.prev_arrow_cat);
                sendBtn.setImageResource(R.drawable.send_cat);
                break;
            case "english":
                prevBtn.setImageResource(R.drawable.prev_arrow_en);
                sendBtn.setImageResource(R.drawable.send_en);
                break;
        }
    }


}
