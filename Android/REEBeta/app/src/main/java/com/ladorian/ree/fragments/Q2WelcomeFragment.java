package com.ladorian.ree.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.models.Cuestionario;

public  class Q2WelcomeFragment extends Fragment {
    private static Typeface miso_font;
    private EditText soy_edit;
    private EditText conoci_edit;
    private Traducciones trad;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Traducciones trad = new Traducciones(getActivity());
        View rootView = inflater.inflate(R.layout.q_welcome_fragment,container,false);

        TextView welcome = (TextView)rootView.findViewById(R.id.welcome);
        welcome.setTypeface(miso_font);
        welcome.setText(trad.getTexto("welcome"));
        ImageButton close = (ImageButton)rootView.findViewById(R.id.close_btn);
        ImageButton next_btn = (ImageButton)rootView.findViewById(R.id.next_btn);

        switch (trad.getIdioma()){
            case "castellano":
                close.setImageResource(R.drawable.close_arrow);
                next_btn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                close.setImageResource(R.drawable.close_arrow_cat);
                next_btn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                close.setImageResource(R.drawable.close_arrow_en);
                next_btn.setImageResource(R.drawable.next_arrow_en);
                break;
        }

        /// para la app en modo kiosko
        ///close.setVisibility(View.GONE);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CuestionarioActivity)getActivity()).changeQuestion(0);
            }
        });
        return rootView;
    }
}