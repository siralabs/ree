package com.ladorian.ree.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GRANBAZU on 27/4/15.
 */
public class QBulbFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    private ImageView background;
    private ImageView white;
    private ImageView light;
    private TextView answer;
    private SeekBar seekBar;
    private Typeface chalk;
    private Typeface miso;
    private int question;
    //private final String[] answers1 = new String[] {"Muy Malo","Malo","Bueno","Muy Bueno"};
    private final List<String> answers1;
    private final List<String> answers2;
    private final List<String> answers3;
    private final List<String> answers4;
    private final Map<String,Map<String,Object>> questions = new HashMap<String,Map<String,Object>> ();
    private Map<String,Object> content = null;
    private Traducciones trad;
    public QBulbFragment() {
        answers1 = Arrays.asList("Muy Malo","Malo","Bueno","Muy Bueno");
        answers2 = Arrays.asList("No sé","Mala","Regular","Buena","Excelente");
        answers3 = Arrays.asList("No sé","Innecesarias","Poco Necesarias","Necesarias","Imprescindibles");
        answers4 = Arrays.asList("No sé","Malo","Regular","Bueno","Excelente");
        Map<String,Object> q2 = new HashMap<>();
        q2.put("number","2");
        q2.put("title","Indique su grado de satisfacción");
        q2.put("question","Opinión general de la exposición");
        q2.put("answers",answers1);
        q2.put("skip",false);
        questions.put("q2",q2);

        Map<String,Object> q3 = new HashMap<>();
        q3.put("number", "3");
        q3.put("title","Indique su grado de satisfacción");
        q3.put("question","Claridad de la información");
        q3.put("answers", answers1);
        q3.put("skip",false);
        questions.put("q3",q3);

        Map<String,Object> q4 = new HashMap<>();
        q4.put("number","4");
        q4.put("title","Valore sólo si ha participado");
        q4.put("question","Explicación de los monitores");
        q4.put("answers",answers1);
        q4.put("skip",true);
        questions.put("q4",q4);

        Map<String,Object> q5 = new HashMap<>();
        q5.put("number","5");
        q5.put("title","Valore sólo si ha participado");
        q5.put("question","Talleres");
        q5.put("answers",answers1);
        q5.put("skip",true);
        questions.put("q5",q5);

        Map<String,Object> q6 = new HashMap<>();
        q6.put("number","6");
        q6.put("title","Valore sólo si ha participado");
        q6.put("question","Guías didácticas");
        q6.put("answers",answers1);
        q6.put("skip",true);
        questions.put("q6",q6);

        Map<String,Object> q7 = new HashMap<>();
        q7.put("number","7");
        q7.put("title","Valore sólo si ha participado");
        q7.put("question","Vídeos");
        q7.put("answers",answers1);
        q7.put("skip",true);
        questions.put("q7",q7);

        Map<String,Object> q9 = new HashMap<>();
        q9.put("number","9");
        q9.put("title","Nos gustaría saber su opinión sobre Red Eléctrica de España");
        q9.put("question","Antes de ver la exposición");
        q9.put("answers",answers2);
        q9.put("skip",false);
        questions.put("q9",q9);

        Map<String,Object> q10 = new HashMap<>();
        q10.put("number","10");
        q10.put("title","Nos gustaría saber su opinión sobre Red Eléctrica de España");
        q10.put("question","Después de ver la exposición");
        q10.put("answers",answers2);
        q10.put("skip",false);
        questions.put("q10",q10);

        Map<String,Object> q11 = new HashMap<>();
        q11.put("number","11");
        q11.put("title","Qué opinión tiene sobre las líneas y subestaciones en alta tensión");
        q11.put("question","Antes de ver la exposición");
        q11.put("answers",answers3);
        q11.put("skip",false);
        questions.put("q11",q11);

        Map<String,Object> q12 = new HashMap<>();
        q12.put("number","12");
        q12.put("title","Qué opinión tiene sobre las líneas y subestaciones en alta tensión");
        q12.put("question","Después de ver la exposición");
        q12.put("answers",answers3);
        q12.put("skip",false);
        questions.put("q12",q12);

        Map<String,Object> q13 = new HashMap<>();
        q13.put("number","13");
        q13.put("title","Valore el compromiso de Red Eléctrica de España con la sostenibilidad y el medio ambiente");
        q13.put("question","");
        q13.put("answers",answers4);
        q13.put("skip",false);
        questions.put("q13",q13);
    }


    public void setQuestion(int question) {
        this.question = question;
    }

    @Override
    public void onResume() {
        super.onResume();
        //View currentView = this.getView();
        int actual = -1;
        switch(question) {
            case 2:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ2();
                break;
            case 3:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ3();
                break;
            case 4:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ4();
                break;
            case 5:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ5();
                break;
            case 6:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ6();
                break;
            case 7:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ7();
                break;
            case 9:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ9();
                break;
            case 10:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ10();
                break;
            case 11:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ11();
                break;
            case 12:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ12();
                break;
            case 13:
                actual = ((CuestionarioActivity)getActivity()).getmCuestionario().getQ13();
                break;

        }
        if (actual >= 0) {
            answer.setText(((List<String>) content.get("answers")).get(actual));
            seekBar.setProgress(actual);
            background.setImageAlpha((255/seekBar.getMax())*actual);
            white.setImageAlpha((255/(seekBar.getMax()))*actual);
            light.setImageAlpha((255/(seekBar.getMax()))*actual);
        } else if (actual == -2 ) {
            answer.setText(trad.getTexto("Sin respuesta"));
            seekBar.setProgress(0);
            background.setImageAlpha(0);
            white.setImageAlpha(0);
            light.setImageAlpha(0);
        } else
        {
            answer.setText(trad.getTexto(((List<String>) content.get("answers")).get(((List<String>) content.get("answers")).size() - 1)));
            seekBar.setProgress(((List<String>)content.get("answers")).size() -1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        trad = new Traducciones(getActivity());

        content = questions.get("q"+Integer.toString(question));
        chalk = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Chalkduster.ttf");
        miso = Typeface.createFromAsset(getActivity().getAssets(),"fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.q_bulb_fragment,container,false);
        ImageButton nextBtn = (ImageButton)rootView.findViewById(R.id.next_btn);
        ImageButton prevBtn = (ImageButton)rootView.findViewById(R.id.prev_btn);
        switch (trad.getIdioma()){
            case "castellano":
                prevBtn.setImageResource(R.drawable.prev_arrow);
                nextBtn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                prevBtn.setImageResource(R.drawable.prev_arrow_cat);
                nextBtn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                prevBtn.setImageResource(R.drawable.prev_arrow_en);
                nextBtn.setImageResource(R.drawable.next_arrow_en);
                break;
        }
        nextBtn.setFocusableInTouchMode(false);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer.getText().equals(trad.getTexto("Sin respuesta"))) {
                    contestar(-2);
                } else {
                    contestar(seekBar.getProgress());
                }

                int newq = question;
                ((CuestionarioActivity) getActivity()).changeQuestion(newq);
            }
        });

        prevBtn.setFocusableInTouchMode(false);
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer.getText().equals(trad.getTexto("Sin respuesta"))) {
                    contestar(-2);
                } else {
                    contestar(seekBar.getProgress());
                }
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });
        TextView title = (TextView)rootView.findViewById(R.id.title);
        title.setTypeface(miso);
        title.setText(trad.getTexto((String) content.get("title")));
        TextView qText = (TextView)rootView.findViewById(R.id.question);
        qText.setTypeface(miso);
        qText.setText(trad.getTexto((String)content.get("question")));
        TextView counter = (TextView)rootView.findViewById(R.id.counter);
        counter.setText(Integer.toString(question)+"/14");
        background = (ImageView)rootView.findViewById(R.id.q_bkg);
        white = (ImageView)rootView.findViewById(R.id.white);
        light = (ImageView)rootView.findViewById(R.id.light);
        seekBar = (SeekBar)rootView.findViewById(R.id.seekBar);
        seekBar.setMax(((List<String>)content.get("answers")).size() -1);
        seekBar.setOnSeekBarChangeListener(this);
        answer = (TextView)rootView.findViewById(R.id.answer);
        answer.setTypeface(chalk);


        Button skipBtn = (Button)rootView.findViewById(R.id.skip_btn);
        skipBtn.setText(trad.getTexto("No contestar").toUpperCase());
        //SEGUIMOS SIN CONTESTAR
        if((Boolean)content.get("skip") == false) {
            skipBtn.setVisibility(View.GONE);
        } else {
            skipBtn.setFocusableInTouchMode(false);
            skipBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    contestar(-2);
                    ((CuestionarioActivity)getActivity()).changeQuestion(question);
                }
            });
        }
        return rootView;
    }

    private void contestar(int valor) {
        switch(question) {
            case 2:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ2(valor);
                break;
            case 3:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ3(valor);
                break;
            case 4:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ4(valor);
                break;
            case 5:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ5(valor);
                break;
            case 6:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ6(valor);
                break;
            case 7:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ7(valor);
                break;
            case 9:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ9(valor);
                break;
            case 10:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ10(valor);
                break;
            case 11:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ11(valor);
                break;
            case 12:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ12(valor);
                break;
            case 13:
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ13(valor);
                break;

        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        background.setImageAlpha((255/seekBar.getMax())*seekBar.getProgress());
        white.setImageAlpha((255/(seekBar.getMax()))*seekBar.getProgress());
        light.setImageAlpha((255/(seekBar.getMax()))*seekBar.getProgress());

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        answer.setText(trad.getTexto(((List<String>) content.get("answers")).get(seekBar.getProgress())));
    }
}
