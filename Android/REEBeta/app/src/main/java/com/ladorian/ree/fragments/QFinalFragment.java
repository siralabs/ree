package com.ladorian.ree.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;

/**
 * Created by GRANBAZU on 28/4/15.
 */
public class QFinalFragment extends Fragment implements TextView.OnEditorActionListener {
    private EditText texto;
    private Typeface miso;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Traducciones trad = new Traducciones(getActivity());
        miso = Typeface.createFromAsset(getActivity().getAssets(),"fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.q_final_fragment,container,false);
        TextView title = (TextView)rootView.findViewById(R.id.title);
        title.setTypeface(miso);
        title.setText(trad.getTexto("comentariosugerencia"));
        ImageButton sendBtn = (ImageButton)rootView.findViewById(R.id.send_btn);
        ImageButton prevBtn = (ImageButton)rootView.findViewById(R.id.prev_btn);
        switch (trad.getIdioma()){
            case "castellano":
                sendBtn.setImageResource(R.drawable.send);
                prevBtn.setImageResource(R.drawable.prev_arrow);
                break;
            case "catala":
                sendBtn.setImageResource(R.drawable.send_cat);
                prevBtn.setImageResource(R.drawable.prev_arrow_cat);
                break;
            case "english":
                sendBtn.setImageResource(R.drawable.send_en);
                prevBtn.setImageResource(R.drawable.prev_arrow_en);
                break;
        }
        texto = (EditText)rootView.findViewById(R.id.editText);
        texto.setOnEditorActionListener(this);
        texto.setRawInputType(InputType.TYPE_CLASS_TEXT);
        texto.setImeOptions(EditorInfo.IME_ACTION_GO);
        sendBtn.setFocusableInTouchMode(false);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CuestionarioActivity)getActivity()).getmCuestionario().setQ14(texto.getText().toString());
                ((CuestionarioActivity)getActivity()).sendCuestionario();
            }
        });
        prevBtn.setFocusableInTouchMode(false);
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });
        return rootView;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            v.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
