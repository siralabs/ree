package com.ladorian.ree.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.NumberPicker;

import com.ladorian.ree.helpers.Traducciones;

/**
 * Created by GRANBAZU on 27/4/15.
 */
public class QFirstDialogFragment extends DialogFragment {
    private String [] soy;
    private String [] conoci;
    public String mProfesion;
    public String mConoci;
    public int content;
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    NoticeDialogListener mListener;

    public void setmListener(NoticeDialogListener mListener) {
        this.mListener = mListener;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public String getmProfesion() {
        return mProfesion;
    }

    public String getmConoci() {
        return mConoci;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Traducciones trad = new Traducciones(getActivity());
        final NumberPicker picker = new NumberPicker(this.getActivity());
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        if(content == 0) {
            soy = new String[] {trad.getTexto("Estudiante"),trad.getTexto("Profesor o profesora"),trad.getTexto("Turista"),trad.getTexto("Otro")};
            picker.setDisplayedValues(soy);
            picker.setMinValue(0);
            picker.setMaxValue(3);
            builder.setMessage(trad.getTexto("Soy..."))
                    .setView(picker)
                    .setPositiveButton(trad.getTexto("Aceptar"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            String[] profesiones = picker.getDisplayedValues();
                            mProfesion = profesiones[picker.getValue()];
                            mListener.onDialogPositiveClick(QFirstDialogFragment.this);
                        }
                    })
                    .setNegativeButton(trad.getTexto("Cancelar"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
        } else {
            conoci = new String[] {trad.getTexto("Internet"),trad.getTexto("Medios de comunicacion"),trad.getTexto("Visita en grupo"),trad.getTexto("Otro")};
            picker.setDisplayedValues(conoci);
            picker.setMinValue(0);
            picker.setMaxValue(3);
            builder.setMessage(trad.getTexto("La conoci en..."))
                    .setView(picker)
                    .setPositiveButton(trad.getTexto("Aceptar"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            String[] profesiones = picker.getDisplayedValues();
                            mConoci = profesiones[picker.getValue()];
                            mListener.onDialogPositiveClick(QFirstDialogFragment.this);
                        }
                    })
                    .setNegativeButton(trad.getTexto("Cancelar"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
        }


        return builder.create();
    }
}
