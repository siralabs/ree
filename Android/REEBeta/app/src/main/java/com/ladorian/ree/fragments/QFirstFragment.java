package com.ladorian.ree.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;

/**
 * Created by GRANBAZU on 27/4/15.
 */
public class QFirstFragment extends Fragment implements QFirstDialogFragment.NoticeDialogListener, TextView.OnEditorActionListener {
    private static Typeface miso_font;
    private Button soyBtn;
    private Button conociBtn;
    private EditText soy_edit;
    private EditText conoci_edit;
    private Traducciones trad;
    private String q0;
    private String q1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        trad = new Traducciones(getActivity());
        miso_font = Typeface.createFromAsset(getActivity().getAssets(),"fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.q_first_fragment,container,false);
        TextView soyTxt = (TextView)rootView.findViewById(R.id.soy);
        soyTxt.setTypeface(miso_font);
        soyTxt.setText(trad.getTexto("soy"));
        TextView conociTxt = (TextView)rootView.findViewById(R.id.conoci);
        conociTxt.setText(trad.getTexto("conoci"));
        conociTxt.setTypeface(miso_font);
        soyBtn = (Button)rootView.findViewById(R.id.soy_btn);
        soyBtn.setText(trad.getTexto("soy_btn"));

        conociBtn = (Button)rootView.findViewById(R.id.conoci_btn);
        conociBtn.setText(trad.getTexto("conoci_btn"));
        if(((CuestionarioActivity) getActivity()).getmCuestionario().getSoy() != null &&
            ((CuestionarioActivity) getActivity()).getmCuestionario().getConoci() != null) {
            soyBtn.setText(((CuestionarioActivity) getActivity()).getmCuestionario().getSoy());
            conociBtn.setText(((CuestionarioActivity) getActivity()).getmCuestionario().getConoci());
        }

        soy_edit = (EditText)rootView.findViewById(R.id.soy_edit);
        soy_edit.setVisibility(View.INVISIBLE);

        soy_edit.setOnEditorActionListener(this);
        soy_edit.setRawInputType(InputType.TYPE_CLASS_TEXT);
        soy_edit.setImeOptions(EditorInfo.IME_ACTION_GO);

        conoci_edit = (EditText)rootView.findViewById(R.id.conoci_edit);
        conoci_edit.setVisibility(View.INVISIBLE);

        conoci_edit.setOnEditorActionListener(this);
        conoci_edit.setRawInputType(InputType.TYPE_CLASS_TEXT);
        conoci_edit.setImeOptions(EditorInfo.IME_ACTION_GO);

        final QFirstFragment weakSelf = this;
        soyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QFirstDialogFragment dialog = new QFirstDialogFragment();
                dialog.setmListener(weakSelf);
                dialog.setContent(0);
                dialog.show(getFragmentManager(),"SoyDialogFragment");
            }
        });
        conociBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QFirstDialogFragment dialog = new QFirstDialogFragment();
                dialog.setmListener(weakSelf);
                dialog.setContent(1);
                dialog.show(getFragmentManager(),"ConociDialogFragment");
            }
        });

        ImageButton next_btn = (ImageButton)rootView.findViewById(R.id.next_btn);
        switch (trad.getIdioma()){
            case "castellano":
                next_btn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                next_btn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                next_btn.setImageResource(R.drawable.next_arrow_en);
                break;
        }
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkAnswers()) {
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setSoy(q0);
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setConoci(q1);
                    ((CuestionarioActivity) getActivity()).changeQuestion(1);
                } else {
                    Toast.makeText(getActivity(),trad.getTexto("please"), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;

    }

    private Boolean checkAnswers() {
        if(q0 != null){
            if(q0.equals(trad.getTexto("Otro"))) {
                if(soy_edit.getText() != null && !soy_edit.getText().toString().equals("")) {
                    q0 = soy_edit.getText().toString();
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        if(q1 != null){
            if(q1.equals(trad.getTexto("Otro"))) {
                if(conoci_edit.getText() != null && !conoci_edit.getText().toString().equals("")) {
                    q1 = conoci_edit.getText().toString();
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Log.i("REE","Positive");
        QFirstDialogFragment d = (QFirstDialogFragment)dialog;
        if(d.getmProfesion() != null) {
            soyBtn.setText(d.getmProfesion());
            if(d.getmProfesion().equals(trad.getTexto("Otro"))) {
                soy_edit.setVisibility(View.VISIBLE);
            } else {
                soy_edit.setVisibility(View.INVISIBLE);
                soy_edit.setText("");
            }
            q0 = d.getmProfesion();
        }

        if(d.getmConoci() != null) {
            conociBtn.setText(d.getmConoci());
            if(d.getmConoci().equals(trad.getTexto("Otro"))) {
                conoci_edit.setVisibility(View.VISIBLE);
            } else {
                conoci_edit.setVisibility(View.INVISIBLE);
                conoci_edit.setText("");
            }
            q1 = d.getmConoci();
        }

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.i("REE","Negative");
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            v.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
