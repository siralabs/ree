package com.ladorian.ree.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.CuestionarioActivity;
import com.ladorian.ree.helpers.Traducciones;
import com.ladorian.ree.models.Cuestionario;

/**
 * Created by GRANBAZU on 28/4/15.
 */
public class QMultiFragment extends Fragment {
    private Typeface miso;
    private Button likesAll;
    private Button dislikesAll;
    private LinearLayout partials;
    private ImageButton refreshOne;
    private ImageButton dislikeOne;
    private ImageButton likeOne;
    private ImageButton refreshTwo;
    private ImageButton dislikeTwo;
    private ImageButton likeTwo;
    private ImageButton refreshThree;
    private ImageButton dislikeThree;
    private ImageButton likeThree;
    private ImageButton refreshFour;
    private ImageButton dislikeFour;
    private ImageButton likeFour;
    private ImageButton refreshLikes;
    private ImageButton refreshDislikes;

    public Boolean likesEverything = false;
    public Boolean likesNothing = false;
    private Traducciones trad;
    public int hasAnswered;

    public int q81;
    public int q82;
    public int q83;
    public int q84;

    @Override
    public void onResume() {
        super.onResume();
        Cuestionario c =  ((CuestionarioActivity)getActivity()).getmCuestionario();
        String mq8 = c.getQ8();
        if(mq8 != null && mq8.equals("TODO")) {
            dislikesAll.setVisibility(View.INVISIBLE);
            partials.setVisibility(View.INVISIBLE);
            refreshLikes.setVisibility(View.VISIBLE);
            refreshDislikes.setVisibility(View.INVISIBLE);
            likesEverything = true;
            likesNothing = false;

            q81 = 0;
            q82 = 0;
            q83 = 0;
            q84 = 0;
        } else if (mq8 != null && mq8.equals("NADA")) {
            likesAll.setVisibility(View.INVISIBLE);
            partials.setVisibility(View.INVISIBLE);
            refreshDislikes.setVisibility(View.VISIBLE);
            refreshLikes.setVisibility(View.INVISIBLE);
            likesNothing = true;
            likesEverything = false;

            q81 = 0;
            q82 = 0;
            q83 = 0;
            q84 = 0;
        } else {
            q81 = c.getQ81();
            q82 = c.getQ82();
            q83 = c.getQ83();
            q84 = c.getQ84();
            refreshDislikes.setVisibility(View.INVISIBLE);
            refreshLikes.setVisibility(View.INVISIBLE);
        }
        checkPartials();
    }

    public void checkPartials() {
        switch(q81) {
            case -1:
                refreshOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                likeOne.setVisibility(View.INVISIBLE);

                break;
            case 0:
                refreshOne.setVisibility(View.INVISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                likeOne.setVisibility(View.VISIBLE);

                break;
            case 1:
                refreshOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.INVISIBLE);
                likeOne.setVisibility(View.VISIBLE);

                break;
        }
        switch(q82) {
            case -1:
                refreshTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                likeTwo.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshTwo.setVisibility(View.INVISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                likeTwo.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.INVISIBLE);
                likeTwo.setVisibility(View.VISIBLE);
                break;
        }
        switch(q83) {
            case -1:
                refreshThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                likeThree.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshThree.setVisibility(View.INVISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                likeThree.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.INVISIBLE);
                likeThree.setVisibility(View.VISIBLE);
                break;
        }
        switch(q84) {
            case -1:
                refreshFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                likeFour.setVisibility(View.INVISIBLE);
                break;
            case 0:
                refreshFour.setVisibility(View.INVISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                likeFour.setVisibility(View.VISIBLE);
                break;
            case 1:
                refreshFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.INVISIBLE);
                likeFour.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        trad = new Traducciones(getActivity());
        miso = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");
        View rootView = inflater.inflate(R.layout.q_multi_fragment,container,false);
        TextView title = (TextView)rootView.findViewById(R.id.title);
        title.setTypeface(miso);
        title.setText(trad.getTexto("Tras visitar la exposicion, por favor, indique lo que mas y lo que menos le ha gustado"));
        ImageButton nextBtn = (ImageButton)rootView.findViewById(R.id.next_btn);
        ImageButton prevBtn = (ImageButton)rootView.findViewById(R.id.prev_btn);
        switch (trad.getIdioma()){
            case "castellano":
                prevBtn.setImageResource(R.drawable.prev_arrow);
                nextBtn.setImageResource(R.drawable.next_arrow);
                break;
            case "catala":
                prevBtn.setImageResource(R.drawable.prev_arrow_cat);
                nextBtn.setImageResource(R.drawable.next_arrow_cat);
                break;
            case "english":
                prevBtn.setImageResource(R.drawable.prev_arrow_en);
                nextBtn.setImageResource(R.drawable.next_arrow_en);
                break;
        }
        nextBtn.setFocusableInTouchMode(false);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasAnswered > 0) {
                    if (likesEverything == true) {
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("TODO");
                    } else if (likesNothing == true) {
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("NADA");
                    } else {
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("");
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ81(q81);
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ82(q82);
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ83(q83);
                        ((CuestionarioActivity) getActivity()).getmCuestionario().setQ84(q84);
                    }
                    ((CuestionarioActivity) getActivity()).changeQuestion(8);
                } else {
                    Toast.makeText(getActivity(), trad.getTexto("Por favor, conteste a esta pregunta para continuar."), Toast.LENGTH_SHORT).show();
                }
            }
        });
        prevBtn.setFocusableInTouchMode(false);
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (likesEverything == true) {
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("TODO");
                } else if (likesNothing == true) {
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("NADA");
                } else {
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ8("");
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ81(q81);
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ82(q82);
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ83(q83);
                    ((CuestionarioActivity) getActivity()).getmCuestionario().setQ84(q84);
                }
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });

        likesAll = (Button)rootView.findViewById(R.id.full_like_btn);
        likesAll.setText(trad.getTexto("Me ha gustado todo"));
        dislikesAll = (Button)rootView.findViewById(R.id.full_dislike_btn);
        dislikesAll.setText(trad.getTexto("No me ha gustado nada"));
        partials = (LinearLayout)rootView.findViewById(R.id.partials);
        refreshOne = (ImageButton)rootView.findViewById(R.id.refresh_one);
        refreshTwo = (ImageButton)rootView.findViewById(R.id.refresh_two);
        refreshThree = (ImageButton)rootView.findViewById(R.id.refresh_three);
        refreshFour = (ImageButton)rootView.findViewById(R.id.refresh_four);
        likeOne = (ImageButton)rootView.findViewById(R.id.like_one);
        likeTwo = (ImageButton)rootView.findViewById(R.id.like_two);
        likeThree = (ImageButton)rootView.findViewById(R.id.like_three);
        likeFour = (ImageButton)rootView.findViewById(R.id.like_four);
        dislikeOne = (ImageButton)rootView.findViewById(R.id.dislike_one);
        dislikeTwo = (ImageButton)rootView.findViewById(R.id.dislike_two);
        dislikeThree = (ImageButton)rootView.findViewById(R.id.dislike_three);
        dislikeFour = (ImageButton)rootView.findViewById(R.id.dislike_four);
        refreshLikes = (ImageButton)rootView.findViewById(R.id.refresh_likes);
        refreshDislikes = (ImageButton)rootView.findViewById(R.id.refresh_dislikes);
        TextView experienciaone = (TextView)rootView.findViewById(R.id.experiencia_one);
        experienciaone.setText(trad.getTexto("Mesa de experimentos"));
        TextView experienciatwo = (TextView)rootView.findViewById(R.id.experiencia_two);
        experienciatwo.setText(trad.getTexto("Maqueta sistema electrico"));
        TextView experienciathree = (TextView)rootView.findViewById(R.id.experiencia_three);
        experienciathree.setText(trad.getTexto("Piezas y material electrico"));
        TextView experienciafour = (TextView)rootView.findViewById(R.id.experiencia_four);
        experienciafour.setText(trad.getTexto("Interactivos"));
        likesAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikesAll.setVisibility(View.INVISIBLE);
                partials.setVisibility(View.INVISIBLE);
                refreshLikes.setVisibility(View.VISIBLE);
                likesEverything = true;
                likesNothing = false;
                hasAnswered++;
            }
        });

        refreshLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikesAll.setVisibility(View.VISIBLE);
                partials.setVisibility(View.VISIBLE);
                refreshLikes.setVisibility(View.INVISIBLE);
                likesEverything = false;
                likesNothing = false;
                hasAnswered--;
            }
        });

        dislikesAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likesAll.setVisibility(View.INVISIBLE);
                partials.setVisibility(View.INVISIBLE);
                refreshDislikes.setVisibility(View.VISIBLE);
                likesNothing = true;
                likesEverything = false;
                hasAnswered++;
            }
        });

        refreshDislikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likesAll.setVisibility(View.VISIBLE);
                partials.setVisibility(View.VISIBLE);
                refreshDislikes.setVisibility(View.INVISIBLE);
                likesNothing = false;
                likesEverything = false;
                hasAnswered--;
            }
        });

        likeOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeOne.setVisibility(View.INVISIBLE);
                refreshOne.setVisibility(View.VISIBLE);
                q81 = 1;
                hasAnswered++;
            }
        });

        dislikeOne.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeOne.setVisibility(View.INVISIBLE);
                refreshOne.setVisibility(View.VISIBLE);
                q81 = -1;
                hasAnswered++;
            }
        });

        refreshOne.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                likeOne.setVisibility(View.VISIBLE);
                dislikeOne.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q81 = 0;
                hasAnswered--;
            }
        });

        likeTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeTwo.setVisibility(View.INVISIBLE);
                refreshTwo.setVisibility(View.VISIBLE);
                q82 = 1;
                hasAnswered++;
            }
        });

        dislikeTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeTwo.setVisibility(View.INVISIBLE);
                refreshTwo.setVisibility(View.VISIBLE);
                q82 = -1;
                hasAnswered++;
            }
        });

        refreshTwo.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                likeTwo.setVisibility(View.VISIBLE);
                dislikeTwo.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q82 = 0;
                hasAnswered--;
            }
        });

        likeThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeThree.setVisibility(View.INVISIBLE);
                refreshThree.setVisibility(View.VISIBLE);
                q83 = 1;
                hasAnswered++;
            }
        });

        dislikeThree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeThree.setVisibility(View.INVISIBLE);
                refreshThree.setVisibility(View.VISIBLE);
                q83 = -1;
                hasAnswered++;
            }
        });

        refreshThree.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                likeThree.setVisibility(View.VISIBLE);
                dislikeThree.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q83 = 0;
                hasAnswered--;
            }
        });

        likeFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikeFour.setVisibility(View.INVISIBLE);
                refreshFour.setVisibility(View.VISIBLE);
                q84 = 1;
                hasAnswered++;
            }
        });

        dislikeFour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                likeFour.setVisibility(View.INVISIBLE);
                refreshFour.setVisibility(View.VISIBLE);
                q84 = -1;
                hasAnswered++;
            }
        });

        refreshFour.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                likeFour.setVisibility(View.VISIBLE);
                dislikeFour.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
                q84 = 0;
                hasAnswered--;
            }
        });

        return rootView;
    }

}
