package com.ladorian.ree.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ladorian.ree.R;
import com.ladorian.ree.activities.MainActivity;


/**
 * Created on 24/10/2015.
 */
public class StartFragment extends Fragment implements AdapterView.OnItemClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity mainActivity = configuraNavegacion(false);
        View rootView = inflater.inflate(R.layout.activity_start, container, false);
        //traducirFragment(mainActivity, rootView);
        configurarBotones(mainActivity, rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        configuraNavegacion(false);
    }

    private MainActivity configuraNavegacion(boolean back) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.hideAllBar();
        mainActivity.hideConfigBtn();
        mainActivity.hideGuideBtn();
        mainActivity.setGoBack(back);
        return mainActivity;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

//    protected void traducirFragment(Activity activity, View view) {
//        Traducciones trad = new Traducciones(activity);
//
//        Typeface miso_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/miso.otf");
//
//    }

    protected void configurarBotones(final MainActivity activity, View view) {
        ImageView playBtn = (ImageView) view.findViewById(R.id.play_btn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.changeFragment(new MainMenuFragment());
                //activity.getSupportFragmentManager().beginTransaction().add(R.id.container, new MainMenuFragment()).commit();
            }
        });

    }
}
