package com.ladorian.ree.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ladorian.ree.models.Ambito;
import com.ladorian.ree.models.Content;
import com.ladorian.ree.models.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by GRANBAZU on 22/4/15.
 */
public class AmbitoItems {
    private Activity activity;
    public Ambito ambito;
    public int aNumber;
    private String idioma;
    public AmbitoItems (Activity a, int index) {
        activity = a;
        SharedPreferences prefs = a.getSharedPreferences("REE_PREFS", Context.MODE_PRIVATE);
        idioma = prefs.getString("idioma",null);
        aNumber = index;
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONObject amb = obj.getJSONArray("ambitos").getJSONObject(aNumber);
            JSONArray items = amb.getJSONArray("items");
            ArrayList<Content> itemList = new ArrayList<Content>();
            for(int i = 0; i< items.length();i++) {
                JSONObject item = items.getJSONObject(i);
                Content c = generateContent(item);
//                c.setTitle(items.getJSONObject(i).getString("title"));
//                c.setIcon(items.getJSONObject(i).getString("icon"));
//                c.setAudio(items.getJSONObject(i).getString("audio"));
//                c.setText(items.getJSONObject(i).getString("text"));
//                c.setIbkg(items.getJSONObject(i).getString("ibkg"));
//                JSONArray contents = items.getJSONObject(i).getJSONArray("contents");
//                c.setContents(contents);
                itemList.add(c);
            }
            ambito = new Ambito();
            ambito.setTitle(amb.getString("title"));
            ambito.setIcon(amb.getString("icon"));
            ambito.setBottom(amb.getString("bottom"));
            ambito.setContents(itemList);
            ambito.setBackground(amb.getString("background"));
            ambito.setIntro(amb.getString("intro"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
 private Content generateContent(JSONObject item){

     Content c = new Content();
     try {
     c.setTitle(item.getString("title"));
     c.setIcon(item.getString("icon"));
     c.setAudio(item.getString("audio"));
     c.setText(item.getString("text"));
     c.setIbkg(item.getString("ibkg"));
     JSONArray contents = item.getJSONArray("contents");
     c.setContents(contents);
 } catch (JSONException e) {
        e.printStackTrace();
    }
     return c;
 }
    private String loadJSONFromAsset() {
        String json = null;
        InputStream is;
        try {
            if(idioma!=null) {
                if (idioma.equals("castellano")) {
                    is = activity.getAssets().open("ambitos_ES.json");
                } else if (idioma.equals("catala")) {
                    is = activity.getAssets().open("ambitos_CAT.json");
                } else {
                    is = activity.getAssets().open("ambitos_EN.json");
                }
            } else {
                is = activity.getAssets().open("ambitos_ES.json");
            }

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
