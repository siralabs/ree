package com.ladorian.ree.helpers;

import android.app.Activity;

import com.ladorian.ree.models.Ambito;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by GRANBAZU on 20/4/15.
 */
public class Ambitos {

    private Activity activity;
    public ArrayList<Ambito> ambitos;
    private String idioma;

    public Ambitos(Activity a,String language) {
        activity = a;
        idioma = language;
        ambitos = new ArrayList<Ambito>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray aList = obj.getJSONArray("ambitos");
             for(int i = 0; i< aList.length();i++) {
                 Ambito amb = new Ambito();
                 amb.setBackground(aList.getJSONObject(i).getString("background"));
                 amb.setBottom(aList.getJSONObject(i).getString("bottom"));
                 amb.setIcon(aList.getJSONObject(i).getString("icon"));
                 amb.setTitle(aList.getJSONObject(i).getString("title"));
                 amb.setIntro(aList.getJSONObject(i).getString("intro"));
                 ambitos.add(amb);
             }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is;
            if(idioma.equals("ES")) {
                is = activity.getAssets().open("ambitos_ES.json");
            } else if (idioma.equals("CAT")) {
                is = activity.getAssets().open("ambitos_CAT.json");
            } else {
                is = activity.getAssets().open("ambitos_EN.json");
            }
            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
