package com.ladorian.ree.helpers;

import com.ladorian.ree.models.Cuestionario;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by GRANBAZU on 28/4/15.
 */
public class ApiClient {
    public static REEApiInterface sREEWS;

    public static REEApiInterface getREEApiClient() {
        if(sREEWS == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("http://www.spoticoins.es/REE")
                    .build();

            sREEWS = restAdapter.create(REEApiInterface.class);
        }

        return sREEWS;
    }

    public interface REEApiInterface {
        @POST("/cuestionario")
        void createCuestionario(@Body Cuestionario qData, Callback<Response> callback);
    }
}
