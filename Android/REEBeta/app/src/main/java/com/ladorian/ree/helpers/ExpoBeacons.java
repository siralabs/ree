package com.ladorian.ree.helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by GRANBAZU on 21/5/15.
 */
public class ExpoBeacons {
    //private final static String[] MACS = new String[] {"F1:4D:3C:AA:14:BF","E9:5B:56:30:82:32","DE:04:E6:DB:65:47"};
    private final static String[] MACS = new String[] {
            "D3:FF:F2:47:36:4F",
            "F3:D9:B4:70:83:FE",
            "E8:B2:C6:63:30:95",
            "D6:58:72:A0:87:15",
            "DC:C4:81:BD:6F:A1",
            "E2:04:E9:39:74:CC",
            "D3:ED:B1:D4:FD:B2",
            "F1:3E:18:D3:93:65",
            "E5:C3:B3:5A:6B:A0",
            "D6:78:FC:84:1F:26",
            "D0:3C:83:36:A1:6E",
            "C1:90:16:2B:C7:0F",
            "C4:BE:0A:F2:6C:19",
            "C0:37:C0:0B:40:00",
            "E4:39:F3:65:93:18",
            "CC:09:28:D9:14:72",
            "C7:61:CD:31:57:C3",
            "E7:31:83:94:43:8D",
            "F8:2F:DC:E3:0E:62",
            "CD:B7:7C:C9:2F:EE",
            "F3:E3:47:81:57:0C",
            "E8:BE:3F:27:EA:A9",
            "C3:03:64:17:5D:76",
            "DE:9D:49:76:02:FA",
            "CC:AA:28:AE:7E:6C",
            "F7:3A:C9:C6:94:92",
            "E9:89:85:B2:8B:B1",
            "FA:D8:8A:02:15:69",
            "C2:64:BB:B1:95:71",
            "EB:40:7C:13:16:61",
            "C1:63:C7:D5:07:61",
            "E7:29:84:48:26:7A",
            "D2:E4:3D:35:40:40",
            "DB:26:23:95:9D:3E"
    };

    public static String[] getMACS() {
        return MACS;
    }

}
