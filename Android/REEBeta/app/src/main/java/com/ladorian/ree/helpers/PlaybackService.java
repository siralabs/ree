package com.ladorian.ree.helpers;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

import com.ladorian.ree.activities.MainActivity;
import com.ladorian.ree.R;

import java.io.IOException;

/**
 * Created by jan on 13/07/15.
 */
public class PlaybackService extends Service implements MediaPlayer.OnCompletionListener {
    public static final String PARAM_CONTENT_URI = "com.ladorian.ree.helpers.PlaybackService.URI";
    public static final String ACTION_PLAY = "com.ladorian.ree.helpers.PlaybackService.PLAY";
    public static final String ACTION_PAUSE = "com.ladorian.ree.helpers.PlaybackService.PAUSE";
    public static final String ACTION_STOP = "com.ladorian.ree.helpers.PlaybackService.STOP";

    private static final String SERVICE_NAME = "com.ladorian.ree.helpers.PlaybackService";
    private static MediaPlayer mMediaPlayer = null;
    private static final int NOTIFICATION_ID = 10;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent == null || intent.getAction() == null || intent.getAction().equals(ACTION_STOP)) {
            stopPlayer();
        } else if (intent.getAction().equals(ACTION_PLAY)) {
            PendingIntent pi = PendingIntent
                    .getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
            Notification notification = new Notification.Builder(getApplicationContext()).setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.playingAudio))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)).setSmallIcon(R.drawable.ic_notification).setContentIntent(pi).build();
//			Notification notification = new Notification();
//			notification.icon = R.mipmap.ic_launcher;
            notification.flags |= Notification.FLAG_ONGOING_EVENT;
//			notification.setLatestEventInfo(getApplicationContext(), "MusicPlayerSample", "Playing: " + songName, pi);
            startForeground(NOTIFICATION_ID, notification);

            try {
                if (mMediaPlayer == null) {
                    mMediaPlayer = new MediaPlayer();
                }

                mMediaPlayer.reset();
                mMediaPlayer.setOnCompletionListener(this);
                String uriPath = intent.getStringExtra(PARAM_CONTENT_URI);
                Uri uri = Uri.parse(uriPath);
                mMediaPlayer.setDataSource(this, uri);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals(ACTION_PAUSE)) {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }


        return START_STICKY;
    }

    public static boolean isPlaying() {
        boolean res = false;

        try {
            res = mMediaPlayer != null && mMediaPlayer.isPlaying();
        } catch (Exception e) {
//			e.printStackTrace();
        }

        return res;
    }

    public static MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    private void stopPlayer() {
        if (mMediaPlayer != null) {
            if (isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        stopForeground(true);
    }

    @Override
    public void onDestroy() {
        stopPlayer();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlayer();
    }
}
