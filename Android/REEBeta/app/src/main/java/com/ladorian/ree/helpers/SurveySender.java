package com.ladorian.ree.helpers;

/**
 * Created on 14/11/2015.
 */


import com.ladorian.ree.models.Cuestionario;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;

public class SurveySender {

    //URL derived from form URL
    public static final String URL = "https://docs.google.com/forms/d/1JrnD8bZCYWPIYwzTc3OxOslP6t-J82RG6yPjaSnGJj8/formResponse";
    //input element ids found from the live form page

    public static final String edad_KEY = "entry_601802038";
    public static final String sexo_KEY = "entry_433295171";
    public static final String soy_KEY = "entry_12307185";
    public static final String conoci_KEY = "entry_1209549896";
    public static final String q2_KEY = "entry_1790761970";
    public static final String q3_KEY = "entry_833366855";
    public static final String q4_KEY = "entry_808732916";
    public static final String q5_KEY = "entry_1818769350";
    public static final String q6_KEY = "entry_1804279674";
    public static final String q7_KEY = "entry_794168025";
    public static final String q8_KEY = "entry_1957408759";
    public static final String q81_KEY = "entry_1173362122";
    public static final String q82_KEY = "entry_1839648711";
    public static final String q83_KEY = "entry_1776746599";
    public static final String q84_KEY = "entry_602330853";
    public static final String q9_KEY = "entry_93390074";
    public static final String q10_KEY = "entry_57558499";
    public static final String q11_KEY = "entry_1089793488";
    public static final String q12_KEY = "entry_1396455487";
    public static final String q13_KEY = "entry_1801509073";
    public static final String q14_KEY = "entry_935658677";
    public static final String plataforma_KEY = "entry_1770153217";
    public String textoOK;
    public String textoNOOK;

    public Boolean sendData(Cuestionario cuestionario) {
        //Create an object for PostDataTask AsyncTask
        //Create an object for PostDataTask AsyncTask
        PostDataTask postDataTask = new PostDataTask();
        postDataTask.cuestionario = cuestionario;
        //execute asynctask
        postDataTask.execute(URL);
        return postDataTask.resultado;


    }

    //AsyncTask to send data as a http POST request
    private class PostDataTask extends AsyncTask<String, Void, Boolean> {
        public Cuestionario cuestionario;
        public Boolean resultado;

        @Override
        protected Boolean doInBackground(String... contactData) {
            Boolean result = true;
            String url = contactData[0];

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            List<BasicNameValuePair> results = new ArrayList<BasicNameValuePair>();
//            results.add(new BasicNameValuePair("entry.0.single", String.valueOf(cuestionario.getEdad())));
//            results.add(new BasicNameValuePair("entry.1.single", cuestionario.getSexo()));
//            results.add(new BasicNameValuePair("entry.2.single", cuestionario.getSoy()));
//            results.add(new BasicNameValuePair("entry.3.single", cuestionario.getConoci()));
//            results.add(new BasicNameValuePair("entry.4.single", String.valueOf(cuestionario.getQ2())));
//            results.add(new BasicNameValuePair("entry.5.single", String.valueOf(cuestionario.getQ3())));
//            results.add(new BasicNameValuePair("entry.6.single", String.valueOf(cuestionario.getQ4())));
//            results.add(new BasicNameValuePair("entry.7.single", String.valueOf(cuestionario.getQ5())));
//            results.add(new BasicNameValuePair("entry.8.single", String.valueOf(cuestionario.getQ6())));
//            results.add(new BasicNameValuePair("entry.9.single", String.valueOf(cuestionario.getQ7())));
//            results.add(new BasicNameValuePair("entry.10.single", cuestionario.getQ8()));
//            results.add(new BasicNameValuePair("entry.11.single", String.valueOf(cuestionario.getQ81())));
//            results.add(new BasicNameValuePair("entry.12.single", String.valueOf(cuestionario.getQ82())));
//            results.add(new BasicNameValuePair("entry.13.single", String.valueOf(cuestionario.getQ83())));
//            results.add(new BasicNameValuePair("entry.14.single", String.valueOf(cuestionario.getQ84())));
//            results.add(new BasicNameValuePair("entry.15.single", String.valueOf(cuestionario.getQ9())));
//            results.add(new BasicNameValuePair("entry.16.single", String.valueOf(cuestionario.getQ10())));
//            results.add(new BasicNameValuePair("entry.17.single", String.valueOf(cuestionario.getQ11())));
//            results.add(new BasicNameValuePair("entry.18.single", String.valueOf(cuestionario.getQ12())));
//            results.add(new BasicNameValuePair("entry.19.single", String.valueOf(cuestionario.getQ13())));
//            results.add(new BasicNameValuePair("entry.20.single", String.valueOf(cuestionario.getQ14())));
//            results.add(new BasicNameValuePair("entry.21.single", String.valueOf(cuestionario.getPlataforma())));

            results.add(new BasicNameValuePair(edad_KEY, String.valueOf(cuestionario.getEdad())));
            results.add(new BasicNameValuePair(sexo_KEY, cuestionario.getSexo()));
            results.add(new BasicNameValuePair(soy_KEY, cuestionario.getSoy()));
            results.add(new BasicNameValuePair(conoci_KEY, cuestionario.getConoci()));
            results.add(new BasicNameValuePair(q2_KEY, String.valueOf(cuestionario.getQ2())));
            results.add(new BasicNameValuePair(q3_KEY, String.valueOf(cuestionario.getQ3())));
            results.add(new BasicNameValuePair(q4_KEY, String.valueOf(cuestionario.getQ4())));
            results.add(new BasicNameValuePair(q5_KEY, String.valueOf(cuestionario.getQ5())));
            results.add(new BasicNameValuePair(q6_KEY, String.valueOf(cuestionario.getQ6())));
            results.add(new BasicNameValuePair(q7_KEY, String.valueOf(cuestionario.getQ7())));
            results.add(new BasicNameValuePair(q8_KEY, cuestionario.getQ8()));
            results.add(new BasicNameValuePair(q81_KEY, String.valueOf(cuestionario.getQ81())));
            results.add(new BasicNameValuePair(q82_KEY, String.valueOf(cuestionario.getQ82())));
            results.add(new BasicNameValuePair(q83_KEY, String.valueOf(cuestionario.getQ83())));
            results.add(new BasicNameValuePair(q84_KEY, String.valueOf(cuestionario.getQ84())));
            results.add(new BasicNameValuePair(q9_KEY, String.valueOf(cuestionario.getQ9())));
            results.add(new BasicNameValuePair(q10_KEY, String.valueOf(cuestionario.getQ10())));
            results.add(new BasicNameValuePair(q11_KEY, String.valueOf(cuestionario.getQ11())));
            results.add(new BasicNameValuePair(q12_KEY, String.valueOf(cuestionario.getQ12())));
            results.add(new BasicNameValuePair(q13_KEY, String.valueOf(cuestionario.getQ13())));
            results.add(new BasicNameValuePair(q14_KEY, String.valueOf(cuestionario.getQ14())));
            results.add(new BasicNameValuePair(plataforma_KEY, String.valueOf(cuestionario.getPlataforma())));
            try {
                post.setEntity(new UrlEncodedFormEntity(results));
            } catch (UnsupportedEncodingException e) {
                // Auto-generated catch block
                e.printStackTrace();
                resultado=false;

                return false;
            }
            try {
                client.execute(post);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                resultado=false;
                return false;
            }
            resultado=true;
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //Print Success or failure message accordingly

            resultado = result;
        }
    }
}
