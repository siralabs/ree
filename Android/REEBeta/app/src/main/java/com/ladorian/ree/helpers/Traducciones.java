package com.ladorian.ree.helpers;

import android.app.Activity;
import android.content.SharedPreferences;

import com.ladorian.ree.models.Traduccion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import java.util.HashMap;
import java.util.Map;

public class Traducciones {

    private Activity activity;
    public Map<String,String> traduccions;
    private String idioma;

    public Traducciones(Activity a) {
        activity = a;
        SharedPreferences prefs = activity.getSharedPreferences("REE_PREFS", 0);
        idioma = prefs.getString("idioma", null);
        if(idioma==null){
            idioma="castellano";
        }
        traduccions = new HashMap<>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray aList = obj.getJSONArray("traducciones");
             for(int i = 0; i< aList.length();i++) {
                 Traduccion trad = new Traduccion();
                 trad.setCampo(aList.getJSONObject(i).getString("campo"));
                 trad.setTexto(aList.getJSONObject(i).getString("texto"));

                 traduccions.put(trad.getCampo(),trad.getTexto());
             }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTexto(String campo){
        String texto = traduccions.get(campo);
        if(texto == null)
        {
            texto = campo;
        }
        return texto;
    }
    public String getIdioma(){
        return this.idioma;
    }
    private String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is;
            if(idioma.equals("castellano")) {
                is = activity.getAssets().open("traducciones_ES.json");
            } else if (idioma.equals("catala")) {
                is = activity.getAssets().open("traducciones_CAT.json");
            } else {
                is = activity.getAssets().open("traducciones_EN.json");
            }
            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "ISO-8859-1");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
