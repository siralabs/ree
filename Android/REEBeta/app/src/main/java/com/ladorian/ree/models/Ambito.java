package com.ladorian.ree.models;

import com.ladorian.ree.models.Content;

import java.util.ArrayList;

/**
 * Created by GRANBAZU on 17/4/15.
 */
public class Ambito {
    String title;
    String background;
    String icon;
    String bottom;
    String intro;
    String ibkg;
    public String getIbkg() {
        return ibkg;
    }

    public void setIbkg(String ibkg) {
        this.ibkg = ibkg;
    }
    ArrayList<Content> contents;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public String getIntro(){return intro;}

    public void setIntro(String intro){this.intro=intro;}



    public ArrayList<Content> getContents() {
        return contents;
    }

    public void setContents(ArrayList<Content> contents) {
        this.contents = contents;
    }
}



