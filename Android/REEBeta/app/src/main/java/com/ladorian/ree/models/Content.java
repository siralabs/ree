package com.ladorian.ree.models;

import com.ladorian.ree.models.Item;

import org.json.JSONArray;

import java.util.ArrayList;

public class Content {
    JSONArray contents;
    String title;
    String icon;
    String audio;
    String text;
    String ibkg;
    public JSONArray getContents() {
        return contents;
    }

    public void setContents(JSONArray contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    public String getIbkg() {
        return ibkg;
    }

    public void setIbkg(String ibkg) {
        this.ibkg = ibkg;
    }
}
