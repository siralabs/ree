package com.ladorian.ree.models;

import android.widget.SeekBar;

/**
 * Created by GRANBAZU on 28/4/15.
 */
public class Cuestionario {
    private String sexo;
    private int edad;
    private String soy;
    private String conoci;
    private int q2 = -1;
    private int q3 = -1;
    private int q4 = -1;
    private int q5 = -1;
    private int q6 = -1;
    private int q7 = -1;
    private String q8;
    private int q81 = 0;
    private int q82 = 0;
    private int q83 = 0;
    private int q84 = 0;
    private int q9 = -1;
    private int q10 = -1;
    private int q11 = -1;
    private int q12 = -1;
    private int q13 = -1;
    private String q14;
    //private static final String plataforma = "android";
    private String plataforma;

    public Cuestionario() {
        //this.plataforma = "exposicion";
        this.plataforma = "android";
        this.setSexo("");
        this.setEdad(0);
        this.setConoci("");
        this.setSoy("");
    }

    public int getQ84() {
        return q84;
    }

    public void setQ84(int q84) {
        this.q84 = q84;
    }

    public int getQ81() {
        return q81;
    }

    public void setQ81(int q81) {
        this.q81 = q81;
    }

    public int getQ82() {
        return q82;
    }

    public void setQ82(int q82) {
        this.q82 = q82;
    }

    public int getQ83() {
        return q83;
    }

    public void setQ83(int q83) {
        this.q83 = q83;
    }

    public String getQ8() {
        return q8;
    }

    public void setQ8(String q8) {
        this.q8 = q8;
    }

    public String getSoy() {
        return soy;
    }

    public void setSoy(String soy) {
        this.soy = soy;
    }

    public String getConoci() {
        return conoci;
    }

    public void setConoci(String conoci) {
        this.conoci = conoci;
    }

    public int getQ2() {
        return q2;
    }

    public void setQ2(int q2) {
        this.q2 = q2;
    }

    public int getQ3() {
        return q3;
    }

    public void setQ3(int q3) {
        this.q3 = q3;
    }

    public int getQ4() {
        return q4;
    }

    public void setQ4(int q4) {
        this.q4 = q4;
    }

    public int getQ5() {
        return q5;
    }

    public void setQ5(int q5) {
        this.q5 = q5;
    }

    public int getQ6() {
        return q6;
    }

    public void setQ6(int q6) {
        this.q6 = q6;
    }

    public int getQ7() {
        return q7;
    }

    public void setQ7(int q7) {
        this.q7 = q7;
    }

    public int getQ9() {
        return q9;
    }

    public void setQ9(int q9) {
        this.q9 = q9;
    }

    public int getQ10() {
        return q10;
    }

    public void setQ10(int q10) {
        this.q10 = q10;
    }

    public int getQ11() {
        return q11;
    }

    public void setQ11(int q11) {
        this.q11 = q11;
    }

    public int getQ12() {
        return q12;
    }

    public void setQ12(int q12) {
        this.q12 = q12;
    }

    public int getQ13() {
        return q13;
    }

    public void setQ13(int q13) {
        this.q13 = q13;
    }

    public String getQ14() {
        return q14;
    }

    public void setQ14(String q14) {
        this.q14 = q14;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public String getSexo(){return sexo;}

    public void setSexo(String sexo){this.sexo=sexo;}

    public int getEdad(){return edad;}
    public void setEdad(int edad){this.edad=edad;}
}
