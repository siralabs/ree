package com.ladorian.ree.models;

/**
 * Created by GRANBAZU on 17/4/15.
 */
public class Item {
    String tipo;
    String archivo;
    String title;
    String ibkg;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getIbkg() {
        return ibkg;
    }

    public void setIbkg(String ibkg) {
        this.ibkg = ibkg;
    }
}
